#!/bin/bash

# First argument - RestAPI ID
# Second argument - Region
# Third argument - Stage name

POSTS_PATH="posts"
POSTS_ID_PARAMETER="id"
POSTS_EDIT_USERNAME_PATH="editDescription"
POSTS_COUNT_PATH="count"
POSTS_OF_USER_PATH="ofUser"

echo "   - Creating resource for path /${POSTS_PATH}, /${POSTS_PATH}/{${POSTS_ID_PARAMETER}}, /${POSTS_PATH}/{${POSTS_ID_PARAMETER}}/${POSTS_EDIT_USERNAME_PATH}, /${POSTS_PATH}/${POSTS_COUNT_PATH}, /${POSTS_PATH}/${POSTS_OF_USER_PATH}"
POSTS_RESOURCE_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${POSTS_PATH}")
RESOURCE_ID_WITH_PARAM_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "{${POSTS_ID_PARAMETER}}" "${POSTS_RESOURCE_ID}")
RESOURCE_ID_WITH_PARAM_ID_EDIT_DESCRIPTION=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${POSTS_EDIT_USERNAME_PATH}" "${RESOURCE_ID_WITH_PARAM_ID}")
RESOURCE_COUNT_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${POSTS_COUNT_PATH}" "${POSTS_RESOURCE_ID}")
RESOURCE_OF_USER_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${POSTS_OF_USER_PATH}" "${POSTS_RESOURCE_ID}")

echo "    - Creating method and integration for GetPostsResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${POSTS_RESOURCE_ID}" "GET" "GetPostsFunction"

echo "    - Creating method and integration for SavePostResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${POSTS_RESOURCE_ID}" "POST" "SavePostFunction"

echo "    - Creating method and integration for DeletePostResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "DELETE" "DeletePostFunction" "${RESOURCE_ID_WITH_PARAM_ID}"

echo "    - Creating method and integration for EditDescriptionResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID_EDIT_DESCRIPTION}" "PUT" "EditDescriptionFunction" "${RESOURCE_ID_WITH_PARAM_ID}"

echo "    - Creating method and integration for GetPostByIdResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "GET" "GetPostByIdFunction" "${RESOURCE_ID_WITH_PARAM_ID}"

echo "    - Creating method and integration for GetNumberOfPostsOfUserResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_COUNT_ID}" "GET" "GetNumberOfPostsOfUserFunction"

echo "    - Creating method and integration for GetPostsOfUserResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_OF_USER_ID}" "GET" "GetPostsOfUserFunction"

echo "   - Base URL: http://localhost:4566/restapis/$1/$3/_user_request_/${POSTS_PATH}"