package zlack.bra.instamini.util;


import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;


public class MultipartUtil {

    private final String boundary;
    private static final String LINE_FEED = "\r\n";
    private ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    private Charset charset = StandardCharsets.UTF_8;

    public MultipartUtil() {
        this.boundary = "---" + System.currentTimeMillis();
    }

    public void addFormField(String name, String value) throws IOException {
        String contentDisposition = "Content-Disposition: form-data; name=\"" + name +"\"";

        outputStream.write("--".getBytes(StandardCharsets.UTF_8));
        outputStream.write(boundary.getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.write(contentDisposition.getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.write("Content-Type: text/plain; charset=".getBytes(StandardCharsets.UTF_8));
        outputStream.write(String.valueOf(charset).getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.write(value.getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));

        outputStream.flush();

        /*result = result + "--" + boundary + LINE_FEED;
        result = result + "Content-Disposition: form-data; name=\"" + name +"\"" + LINE_FEED;
        result = result + "Content-Type: text/plain; charset=" + charset + LINE_FEED;
        result = result + LINE_FEED;
        result = result + value + LINE_FEED;*/
    }

    public void addPngFilePart(String fieldName, File uploadFile)
            throws IOException {
        String fileName = uploadFile.getName();
        String contentDisposition =  "Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"";

        outputStream.write("--".getBytes(StandardCharsets.UTF_8));
        outputStream.write(boundary.getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.write(contentDisposition.getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.write("Content-Type: image/png".getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.write("Content-Transfer-Encoding: binary".getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));

        FileInputStream inputStream = new FileInputStream(uploadFile);
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            outputStream.write(buffer, 0, bytesRead);
            //result = result + new String(Arrays.copyOfRange(buffer, 0, bytesRead));
        }
        inputStream.close();

        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();
        /*result = result + "--" + boundary + LINE_FEED;
        result = result + "Content-Disposition: form-data; name=\"" + fieldName + "\"; filename=\"" + fileName + "\"" + LINE_FEED;
        result = result + "Content-Type: image/png" + LINE_FEED;
        result = result + "Content-Transfer-Encoding: binary" + LINE_FEED;
        result = result + LINE_FEED;

        FileInputStream inputStream = new FileInputStream(uploadFile);
        byte[] buffer = new byte[4096];
        int bytesRead = -1;
        while ((bytesRead = inputStream.read(buffer)) != -1) {
            result = result + new String(Arrays.copyOfRange(buffer, 0, bytesRead));
        }
        inputStream.close();

        result = result + LINE_FEED;*/
    }

    public void finish() throws IOException {
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        outputStream.flush();

        outputStream.write("--".getBytes(StandardCharsets.UTF_8));
        outputStream.write(boundary.getBytes(StandardCharsets.UTF_8));
        outputStream.write("--".getBytes(StandardCharsets.UTF_8));
        outputStream.write(LINE_FEED.getBytes(StandardCharsets.UTF_8));
        /*result = result + LINE_FEED;
        result = result + "--" + boundary + "--" + LINE_FEED;*/
    }

    public String getBoundary() {
        return boundary;
    }

    public byte[] getBytes() {
        return outputStream.toByteArray();
    }
}