package zlack.bra.instamini.data.comment;

public final class Comment {

    private String id;
    private String text;
    private String time;
    private String userId;
    private String postId;

    public Comment() {
    }

    public Comment(String id, String text, String time, String userId, String postId) {
        this.id = id;
        this.text = text;
        this.time = time;
        this.userId = userId;
        this.postId = postId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }
}
