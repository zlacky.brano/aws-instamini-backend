package zlack.bra.instamini.delete;

import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.delete.exception.DeleteChildrenException;
import zlack.bra.instamini.delete.util.ManageBatchWriteItem;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.InvokeLambda;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Deletes all children of post
 */
public class DeletePostChildren {

    private final static ObjectMapper objectMapper = InstanceModule.objectMapper;

    public static void delete(String postId) throws DeleteChildrenException, IOException {
        deleteComments(postId);
        deleteLikes(postId);
    }

    private static void deleteLikes(String postId) throws DeleteChildrenException, IOException {
        InvokeResult result = InvokeLambda.invokeWithQueryParam("GetLikesOnPostFunction", "postId", postId);
        if (result.getFunctionError() != null) {
            throw new DeleteChildrenException("Error, while invoking GetLikesOnPostFunction: " + result.getFunctionError());
        }

        int statusCode = objectMapper.readTree(new ByteBufferBackedInputStream(result.getPayload())).get("statusCode").asInt();
        if (statusCode != 200) {
            throw new DeleteChildrenException("Status code is not equal to 200, when invoking GetLikesOnPostFunction, but instead it is: " + statusCode);
        }

        String jsonBody = objectMapper.readTree(new String(result.getPayload().array())).get("body").toString();
        jsonBody = jsonBody.substring(1, jsonBody.length() - 1).replaceAll("\\\\\"", "\""); // Get rid of double quotes on beginning and end + backslashes at the start of fields
        Like[] likes = objectMapper.readValue(jsonBody, Like[].class);
        List<String> likesIds = new ArrayList<>();

        if (likes.length == 0) {
            return;
        }

        for (Like like : likes) {
            likesIds.add(like.getId());
        }

        TableWriteItems writeItems = new TableWriteItems(TableName.likeTable)
                .withHashOnlyKeysToDelete("id", likesIds.toArray());

        ManageBatchWriteItem.manage(writeItems);
    }

    private static void deleteComments(String postId) throws DeleteChildrenException, IOException {
        InvokeResult result = InvokeLambda.invokeWithQueryParam("CommentsOnPostFunction", "postId", postId);
        if (result.getFunctionError() != null) {
            throw new DeleteChildrenException("Error, while invoking CommentsOnPostFunction: " + result.getFunctionError());
        }

        int statusCode = objectMapper.readTree(new ByteBufferBackedInputStream(result.getPayload())).get("statusCode").asInt();
        if (statusCode != 200) {
            throw new DeleteChildrenException("Status code is not equal to 200, when invoking CommentsOnPostFunction, but instead it is: " + statusCode);
        }

        String jsonBody = objectMapper.readTree(new String(result.getPayload().array())).get("body").toString();
        jsonBody = jsonBody.substring(1, jsonBody.length() - 1).replaceAll("\\\\\"", "\""); // Get rid of double quotes on beginning and end + backslashes at the start of fields
        Comment[] comments = objectMapper.readValue(jsonBody, Comment[].class);
        List<String> commentsIds = new ArrayList<>();

        if (comments.length == 0) {
            return;
        }

        for (Comment comment : comments) {
            commentsIds.add(comment.getId());
        }

        TableWriteItems writeItems = new TableWriteItems(TableName.commentTable)
                .withHashOnlyKeysToDelete("id", commentsIds.toArray());

        ManageBatchWriteItem.manage(writeItems);
    }
}
