package zlack.bra.instamini.test.assertions;

import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.data.post.PostCreate;

public class AssertPosts {

    public static boolean postsEquals(Post post1, Post post2) {
        return post1.getId().equals(post2.getId()) &&
                post1.getPhotoUrl().equals(post2.getPhotoUrl()) &&
                post1.getUserId().equals(post2.getUserId()) &&
                post1.getDescription().equals(post2.getDescription()) &&
                post1.getTime().equals(post2.getTime());
    }

    public static boolean postsEquals(Post post1, Post post2, String newDescription) {
        return post1.getId().equals(post2.getId()) &&
                post1.getPhotoUrl().equals(post2.getPhotoUrl()) &&
                post1.getUserId().equals(post2.getUserId()) &&
                newDescription.equals(post2.getDescription()) &&
                post1.getTime().equals(post2.getTime());
    }

    public static boolean postsEquals(PostCreate postCreate, Post post) {
        return !post.getId().isBlank() &&
                !post.getPhotoUrl().isBlank() &&
                postCreate.getUserId().equals(post.getUserId()) &&
                postCreate.getDescription().equals(post.getDescription()) &&
                !post.getTime().isBlank();
    }
}
