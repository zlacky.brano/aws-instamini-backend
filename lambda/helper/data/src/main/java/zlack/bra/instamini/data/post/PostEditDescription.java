package zlack.bra.instamini.data.post;

public final class PostEditDescription {

    private String description;

    public PostEditDescription() {
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
