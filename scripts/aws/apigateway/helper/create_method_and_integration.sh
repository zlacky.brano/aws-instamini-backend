#!/bin/bash

# First argument - RestAPI ID
# Second argument - Region
# Third argument - Resource ID
# Fourth argument - Method
# Fifth argument - Name of Lambda function
# Sixth argument (Optional) - Name of parameter (null if parameter does not exist)

if [ -z "$6" ]
  then
    docker-compose run --rm aws-cli apigateway put-method --region "$2" --rest-api-id "$1" --resource-id "$3" --http-method "$4" --authorization-type "NONE" --endpoint-url=http://localstack:4566 1>/dev/null
  else
    docker-compose run --rm aws-cli apigateway put-method --region "$2" --rest-api-id "$1" --resource-id "$3" --http-method "$4" --authorization-type "NONE" --request-parameters method.request.path."$6"=true --endpoint-url=http://localstack:4566 1>/dev/null
fi

docker-compose run --rm aws-cli apigateway put-integration --region "$2" --rest-api-id "$1" --resource-id "$3" --http-method "$4" --type AWS_PROXY --integration-http-method "$4" --uri arn:aws:apigateway:"$2":lambda:path/2015-03-31/functions/arn:aws:lambda:"$2":000000000000:function:"$5"/invocations --passthrough-behavior WHEN_NO_MATCH --endpoint-url=http://localstack:4566 1>/dev/null