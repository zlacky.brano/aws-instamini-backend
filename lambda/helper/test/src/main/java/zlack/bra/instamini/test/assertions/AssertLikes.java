package zlack.bra.instamini.test.assertions;

import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.data.like.LikeCreate;

public class AssertLikes {

    public static boolean likesEquals(Like like1, Like like2) {
        return like1.getId().equals(like2.getId()) &&
                like1.getUserId().equals(like2.getUserId()) &&
                like1.getPostId().equals(like2.getPostId());
    }

    public static boolean likesEquals(LikeCreate likeCreate, Like like) {
        return !like.getId().isBlank() &&
                like.getUserId().equals(likeCreate.getUserId()) &&
                like.getPostId().equals(likeCreate.getPostId());
    }
}
