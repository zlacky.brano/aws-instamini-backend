package zlack.bra.instamini;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.PrimaryKey;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.instance.InstanceModule;


public class GetCommentByIdHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final DynamoDB dynamoDB = InstanceModule.dynamoDB;

    /**
     * Gets comment by id
     * @param event all information about request
     * @param context request context
     * @return response with 200, if it was successful or 404, if comment was not found
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            Table table = dynamoDB.getTable(TableName.commentTable);

            Map<String, String> pathParameters = event.getPathParameters();

            Item item = table.getItem(new PrimaryKey("id", pathParameters.get("id")));

            if (item == null) {
                return response.withStatusCode(404);
            }
            return response.withStatusCode(200).withBody(item.toJSONPretty());
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
