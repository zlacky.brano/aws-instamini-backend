#!/bin/bash

# First argument - RestAPI ID
# Second argument - Region
# Third argument - Stage name

COMMENTS_PATH="comments"
COMMENTS_ID_PARAMETER="id"
COMMENTS_EDIT_USERNAME_PATH="editText"
COMMENTS_ON_POST_PATH="onPost"

echo "   - Creating resource for path /${COMMENTS_PATH}, /${COMMENTS_PATH}/{${COMMENTS_ID_PARAMETER}}, /${COMMENTS_PATH}/{${COMMENTS_ID_PARAMETER}}/${COMMENTS_EDIT_USERNAME_PATH}, /${COMMENTS_PATH}/${COMMENTS_ON_POST_PATH}"
COMMENTS_RESOURCE_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${COMMENTS_PATH}")
RESOURCE_ID_WITH_PARAM_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "{${COMMENTS_ID_PARAMETER}}" "${COMMENTS_RESOURCE_ID}")
RESOURCE_ID_WITH_PARAM_ID_EDIT_TEXT=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${COMMENTS_EDIT_USERNAME_PATH}" "${RESOURCE_ID_WITH_PARAM_ID}")
RESOURCE_ID_ON_POST=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${COMMENTS_ON_POST_PATH}" "${COMMENTS_RESOURCE_ID}")

echo "    - Creating method and integration for GetCommentsResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${COMMENTS_RESOURCE_ID}" "GET" "GetCommentsFunction"

echo "    - Creating method and integration for SaveCommentResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${COMMENTS_RESOURCE_ID}" "POST" "SaveCommentFunction"

echo "    - Creating method and integration for DeleteCommentResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "DELETE" "DeleteCommentFunction" "${RESOURCE_ID_WITH_PARAM_ID}"

echo "    - Creating method and integration for EditTextResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID_EDIT_TEXT}" "PUT" "EditTextFunction" "${RESOURCE_ID_WITH_PARAM_ID}"

echo "    - Creating method and integration for GetCommentByIdResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "GET" "GetCommentByIdFunction" "${RESOURCE_ID_WITH_PARAM_ID}"

echo "    - Creating method and integration for GetCommentsResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_ON_POST}" "GET" "CommentsOnPostFunction"

echo "   - Base URL: http://localhost:4566/restapis/$1/$3/_user_request_/${COMMENTS_PATH}"