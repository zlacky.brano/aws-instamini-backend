package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.data.comment.CommentCreate;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.CheckIfDocumentIdExists;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertComments;
import zlack.bra.instamini.test.item.CreateItem;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Testcontainers
class SaveCommentHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private SaveCommentHandler saveCommentHandler;

    private final Comment commentAlreadyIn = new Comment("in", "in", "in", "in", "in");

    private final CommentCreate commentCreate = new CommentCreate("not", "user", "post");

    private final CommentCreate commentCreate404User = new CommentCreate("not", "wrong", "post");

    private final CommentCreate commentCreate404Post = new CommentCreate("not", "user", "wrong");

    private final User user = new User("user", "user", "user");

    private final Post post = new Post("post", "post", "post", "post", "post");



    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        saveCommentHandler = new SaveCommentHandler();

        Table commentTable = CreateTable.create(dynamoDB, TableName.commentTable);
        Table userTable = CreateTable.create(dynamoDB, TableName.userTable);
        Table postTable = CreateTable.create(dynamoDB, TableName.postTable);

        userTable.putItem(CreateItem.createUserItem(user));
        postTable.putItem(CreateItem.createPostItem(post));
        commentTable.putItem(CreateItem.createCommentItem(commentAlreadyIn));
    }

    @Test
    void handleRequest() {
        String invokeResultUserJson = "{\"statusCode\":200}";
        InvokeResult invokeResultUser = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserJson.getBytes()));

        String invokeResultPostJson = "{\"statusCode\":200}";
        InvokeResult invokeResultPost = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultPostJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfPostExists(commentCreate.getPostId()))
                    .thenReturn(invokeResultPost);
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(commentCreate.getUserId()))
                    .thenReturn(invokeResultUser);

            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withBody(commentCreate.toJson());
            APIGatewayProxyResponseEvent response = saveCommentHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(201, response.getStatusCode());

            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(commentCreate.getUserId()));
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfPostExists(commentCreate.getPostId()));
        }

        checkComments();
    }

    @Test
    void handleRequest404User() {
        String invokeResultUserJson = "{\"statusCode\":404}";
        InvokeResult invokeResultUser = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserJson.getBytes()));

        String invokeResultPostJson = "{\"statusCode\":200}";
        InvokeResult invokeResultPost = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultPostJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfPostExists(commentCreate404User.getPostId()))
                    .thenReturn(invokeResultPost);
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(commentCreate404User.getUserId()))
                    .thenReturn(invokeResultUser);
            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withBody(commentCreate404User.toJson());
            APIGatewayProxyResponseEvent response = saveCommentHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(404, response.getStatusCode());
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(commentCreate404User.getUserId()));
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfPostExists(commentCreate404User.getPostId()));
        }
    }

    @Test
    void handleRequest404Post() {
        String invokeResultUserJson = "{\"statusCode\":200}";
        InvokeResult invokeResultUser = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserJson.getBytes()));

        String invokeResultPostJson = "{\"statusCode\":404}";
        InvokeResult invokeResultPost = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultPostJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfPostExists(commentCreate404Post.getPostId()))
                    .thenReturn(invokeResultPost);
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(commentCreate404Post.getUserId()))
                    .thenReturn(invokeResultUser);
            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withBody(commentCreate404Post.toJson());
            APIGatewayProxyResponseEvent response = saveCommentHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(404, response.getStatusCode());
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(commentCreate404Post.getUserId()));
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfPostExists(commentCreate404Post.getPostId()));
        }
    }

    private void checkComments() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.commentTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Comment> comments = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Comment comment = new Comment(item.get("id").getS(), item.get("text").getS(), item.get("time").getS(), item.get("userId").getS(), item.get("postId").getS());
            comments.add(comment);
        }

        Assertions.assertEquals(2, comments.size());
        if (!comments.get(0).getId().equals(commentAlreadyIn.getId())) {
            if (!AssertComments.commentsEquals(commentCreate, comments.get(0)) ||
                    !AssertComments.commentsEquals(commentAlreadyIn, comments.get(1))) {
                throw new AssertionFailedError();
            }
        } else if (comments.get(0).getId().equals(commentAlreadyIn.getId())) {
            if (!AssertComments.commentsEquals(commentCreate, comments.get(1)) ||
                    !AssertComments.commentsEquals(commentAlreadyIn, comments.get(0))) {
                throw new AssertionFailedError();
            }
        } else {
            throw new AssertionFailedError();
        }
    }
}