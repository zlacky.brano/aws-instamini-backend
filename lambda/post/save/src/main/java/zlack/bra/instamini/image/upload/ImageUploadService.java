package zlack.bra.instamini.image.upload;

import zlack.bra.instamini.image.upload.exception.UploadException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Map;

public interface ImageUploadService {

    String upload(byte[] file, Map options) throws IOException, UploadException;

    String upload(byte[] file) throws IOException, UploadException;

    void destroy(String publicId, Map options) throws IOException;

    void destroy(String publicId) throws IOException;
}
