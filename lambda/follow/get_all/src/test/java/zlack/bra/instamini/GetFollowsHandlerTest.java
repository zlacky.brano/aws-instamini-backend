package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.follow.Follow;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertFollows;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.List;


@Testcontainers
class GetFollowsHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetFollowsHandler getFollowsHandler;

    private final List<Follow> follows = new ArrayList<>() {
        {
            add(new Follow("id", "test1", "test2"));
            add(new Follow("not", "not2", "not1"));
            add(new Follow("1", "2", "3"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getFollowsHandler = new GetFollowsHandler();

        Table table = CreateTable.create(dynamoDB, TableName.followTable);

        for (Follow follow : follows) {
            table.putItem(CreateItem.createFollowItem(follow));
        }
    }

    @Test
    void handleRequest() throws JsonProcessingException {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent();
        APIGatewayProxyResponseEvent response = getFollowsHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkFollows(response.getBody());
    }

    private void checkFollows(String bodyResult) throws JsonProcessingException {
        Follow[] followsResult = new ObjectMapper().readValue(bodyResult, Follow[].class);

        Assertions.assertEquals(3, followsResult.length);
        boolean found;
        for (Follow followResult : followsResult) {
            found = false;
            for (Follow follow : follows) {
                if (follow.getId().equals(followResult.getId())) {
                    found = true;
                    if (!AssertFollows.followsEquals(followResult, follow)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }
}