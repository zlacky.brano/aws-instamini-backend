package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertUsers;
import zlack.bra.instamini.test.item.CreateItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
class GetUserByIdHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetUserByIdHandler getUserByIdHandler;

    private final List<User> users = new ArrayList<>() {
        {
            add(new User("id", "test", "test"));
            add(new User("not", "not", "not"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getUserByIdHandler = new GetUserByIdHandler();

        Table table = CreateTable.create(dynamoDB, TableName.userTable);

        for (User user : users) {
            table.putItem(CreateItem.createUserItem(user));
        }
    }

    @Test
    void handleRequest() throws IOException {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", users.get(0).getId());
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters);
        APIGatewayProxyResponseEvent response = getUserByIdHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());
        User userResult = new ObjectMapper().readValue(response.getBody(), User.class);
        if (!AssertUsers.usersEquals(users.get(0), userResult)) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void handleRequest404() {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", "does not exist");
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters);
        APIGatewayProxyResponseEvent response = getUserByIdHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(404, response.getStatusCode());
    }
}