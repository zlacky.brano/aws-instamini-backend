package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertPosts;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
class GetPostsOnPostHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetPostsOfUserHandler getPostsOfUserHandler;

    private final List<Post> posts = new ArrayList<>() {
        {
            add(new Post("id", "test", "test", "test", "yes"));
            add(new Post("not", "not", "not", "not", "not"));
            add(new Post("1", "2", "3", "4", "yes"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getPostsOfUserHandler = new GetPostsOfUserHandler();

        Table table = CreateTable.create(dynamoDB, TableName.postTable);

        for (Post post : posts) {
            table.putItem(CreateItem.createPostItem(post));
        }
    }

    @Test
    void handleRequest() throws JsonProcessingException {
        Map<String, String> queryParameters = new HashMap<>() {
            {
                put("userId", "yes");
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(queryParameters);
        APIGatewayProxyResponseEvent response = getPostsOfUserHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkPosts(response.getBody());
    }

    @Test
    void handleRequest400() {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(new HashMap<>());
        APIGatewayProxyResponseEvent response = getPostsOfUserHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(400, response.getStatusCode());
    }

    private void checkPosts(String bodyResult) throws JsonProcessingException {
        Post[] postsResult = new ObjectMapper().readValue(bodyResult, Post[].class);

        Assertions.assertEquals(2, postsResult.length);
        boolean found;
        for (Post postResult : postsResult) {
            found = false;
            for (Post post : posts) {
                if (post.getId().equals(postResult.getId()) && !post.getUserId().equals("not")) {
                    found = true;
                    if (!AssertPosts.postsEquals(postResult, post)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }
}