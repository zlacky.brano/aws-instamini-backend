package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertUsers;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
class GetUsersWhoLikedPostHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetUsersWhoLikedPostHandler getUsersWhoLikedPostHandler;

    private final List<User> users = new ArrayList<>() {
        {
            add(new User("id", "test", "test"));
            add(new User("not1", "not", "not"));
            add(new User("not2", "2", "3"));
        }
    };

    private final List<Like> likes = new ArrayList<>() {
        {
            add(new Like("id1", "id", "yes"));
            add(new Like("id2", "not1", "not"));
            add(new Like("id3", "not1", "3"));
            add(new Like("id4", "not2", "4"));
            add(new Like("id5", "not2", "5"));
            add(new Like("id6", "not2", "6"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getUsersWhoLikedPostHandler = new GetUsersWhoLikedPostHandler();

        Table userTable = CreateTable.create(dynamoDB, TableName.userTable);
        Table likeTable = CreateTable.create(dynamoDB, TableName.likeTable);

        for (User user : users) {
            userTable.putItem(CreateItem.createUserItem(user));
        }
        for (Like like : likes) {
            likeTable.putItem(CreateItem.createLikeItem(like));
        }
    }

    @Test
    void handleRequest() throws JsonProcessingException {
        Map<String, String> queryParameters = new HashMap<>() {
            {
                put("postId", "yes");
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(queryParameters);
        APIGatewayProxyResponseEvent response = getUsersWhoLikedPostHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkUsers(response.getBody());
    }

    @Test
    void handleRequest400() {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(new HashMap<>());
        APIGatewayProxyResponseEvent response = getUsersWhoLikedPostHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(400, response.getStatusCode());
    }

    private void checkUsers(String bodyResult) throws JsonProcessingException {
        User[] usersResult = new ObjectMapper().readValue(bodyResult, User[].class);

        Assertions.assertEquals(1, usersResult.length);
        if (!AssertUsers.usersEquals(usersResult[0], users.get(0))) {
            throw new AssertionFailedError();
        }
    }
}