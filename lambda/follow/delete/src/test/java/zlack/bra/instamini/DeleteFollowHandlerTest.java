package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.data.follow.Follow;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertComments;
import zlack.bra.instamini.test.assertions.AssertFollows;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
class DeleteFollowHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private DeleteFollowHandler deleteFollowHandler;

    private final Follow followToDelete = new Follow("id", "test1", "test2");

    private final Follow follow = new Follow("not", "not1", "not2");

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        deleteFollowHandler = new DeleteFollowHandler();

        Item followToDeleteItem = CreateItem.createFollowItem(followToDelete);
        Item followItem = CreateItem.createFollowItem(follow);

        Table table = CreateTable.create(dynamoDB, TableName.followTable);
        table.putItem(followToDeleteItem);
        table.putItem(followItem);
    }

    @Test
    public void handleRequest() {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", followToDelete.getId());
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters);
        APIGatewayProxyResponseEvent response = deleteFollowHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkDeleted();
    }

    private void checkDeleted() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.followTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Follow> follows = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Follow follow = new Follow(item.get("id").getS(), item.get("userIdWho").getS(), item.get("userIdWhom").getS());
            follows.add(follow);
        }

        Assertions.assertEquals(1, follows.size());
        if (!AssertFollows.followsEquals(follow, follows.get(0))) {
            throw new AssertionFailedError();
        }
    }
}