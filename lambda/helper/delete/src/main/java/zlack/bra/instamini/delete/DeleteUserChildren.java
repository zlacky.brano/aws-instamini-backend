package zlack.bra.instamini.delete;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.data.follow.Follow;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.delete.exception.DeleteChildrenException;
import zlack.bra.instamini.delete.util.ManageBatchWriteItem;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.InvokeLambda;

import java.io.IOException;
import java.util.*;

import static java.util.Map.of;

/**
 * Deletes all children of user
 */
public final class DeleteUserChildren {

    private final static ObjectMapper objectMapper = InstanceModule.objectMapper;

    private final static AmazonDynamoDB amazonDynamoDb = InstanceModule.amazonDynamoDB;

    public static void delete(String userId) throws DeleteChildrenException, IOException {
        deleteFollows(userId);
        deleteLikes(userId);
        deleteComments(userId);
        deletePosts(userId);
    }

    private static void deletePosts(String userId) throws DeleteChildrenException, IOException {
        InvokeResult result = InvokeLambda.invokeWithQueryParam("GetPostsOfUserFunction", "userId", userId);
        if (result.getFunctionError() != null) {
            throw new DeleteChildrenException("Error, while invoking GetPostsOfUserFunction: " + result.getFunctionError());
        }

        int statusCode = objectMapper.readTree(new ByteBufferBackedInputStream(result.getPayload())).get("statusCode").asInt();
        if (statusCode != 200) {
            throw new DeleteChildrenException("Status code is not equal to 200, when invoking GetPostsOfUserFunction, but instead it is: " + statusCode);
        }

        String jsonBody = objectMapper.readTree(new String(result.getPayload().array())).get("body").toString();
        jsonBody = jsonBody.substring(1, jsonBody.length() - 1).replaceAll("\\\\\"", "\""); // Get rid of double quotes on beginning and end + backslashes at the start of fields
        Post[] posts = objectMapper.readValue(jsonBody, Post[].class);
        List<String> postsIds = new ArrayList<>();

        if (posts.length == 0) {
            return;
        }

        for (Post post : posts) {
            DeletePostChildren.delete(post.getId());
            postsIds.add(post.getId());
        }

        TableWriteItems writeItems = new TableWriteItems(TableName.postTable)
                .withHashOnlyKeysToDelete("id", postsIds.toArray());

        ManageBatchWriteItem.manage(writeItems);
    }

    private static void deleteFollows(String userId) {
        Map<String, String> expressionNames = new HashMap<>();
        expressionNames.put("#uWho", "userIdWho");
        expressionNames.put("#uWhom", "userIdWhom");

        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.followTable)
                .withFilterExpression("#uWho = :userId or #uWhom = :userId")
                .withExpressionAttributeNames(expressionNames)
                .withExpressionAttributeValues(of(
                        ":userId", new AttributeValue(userId)
                ));

        ScanResult scanResult = amazonDynamoDb.scan(scanRequest);
        List<Follow> follows = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Follow follow = new Follow(item.get("id").getS(), item.get("userIdWho").getS(), item.get("userIdWhom").getS());
            follows.add(follow);
        }
        List<String> followsIds = new ArrayList<>();

        if (follows.isEmpty()) {
            return;
        }

        for (Follow follow : follows) {
            followsIds.add(follow.getId());
        }

        TableWriteItems writeItems = new TableWriteItems(TableName.followTable)
                .withHashOnlyKeysToDelete("id", followsIds.toArray());

        ManageBatchWriteItem.manage(writeItems);
    }

    private static void deleteComments(String userId) {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.commentTable)
                .withFilterExpression("#u = :userId")
                .withExpressionAttributeNames(of("#u", "userId"))
                .withExpressionAttributeValues(of(
                        ":userId", new AttributeValue(userId)
                ));

        ScanResult scanResult = amazonDynamoDb.scan(scanRequest);
        List<Comment> comments = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Comment comment = new Comment(item.get("id").getS(), item.get("text").getS(), item.get("time").getS(), item.get("userId").getS(), item.get("postId").getS());
            comments.add(comment);
        }
        List<String> commentsIds = new ArrayList<>();

        if (comments.isEmpty()) {
            return;
        }

        for (Comment comment : comments) {
            commentsIds.add(comment.getId());
        }

        TableWriteItems writeItems = new TableWriteItems(TableName.commentTable)
                .withHashOnlyKeysToDelete("id", commentsIds.toArray());

        ManageBatchWriteItem.manage(writeItems);
    }

    private static void deleteLikes(String userId) {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.likeTable)
                .withFilterExpression("#u = :userId")
                .withExpressionAttributeNames(of("#u", "userId"))
                .withExpressionAttributeValues(of(
                        ":userId", new AttributeValue(userId)
                ));

        ScanResult scanResult = amazonDynamoDb.scan(scanRequest);
        List<Like> likes = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Like like = new Like(item.get("id").getS(), item.get("userId").getS(), item.get("postId").getS());
            likes.add(like);
        }
        List<String> likesIds = new ArrayList<>();

        if (likes.isEmpty()) {
            return;
        }

        for (Like like : likes) {
            likesIds.add(like.getId());
        }

        TableWriteItems writeItems = new TableWriteItems(TableName.likeTable)
                .withHashOnlyKeysToDelete("id", likesIds.toArray());

        ManageBatchWriteItem.manage(writeItems);
    }
}
