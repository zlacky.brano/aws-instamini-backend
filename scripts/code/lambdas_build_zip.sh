#!/bin/bash

echo "----- Building Lambdas -----"; echo

./gradlew buildZip
#./gradlew :lambda_user_get_all:buildZip
#./gradlew :lambda_user_save:buildZip
#./gradlew :lambda_user_delete:buildZip
#./gradlew :lambda_user_edit_username:buildZip
#./gradlew :lambda_user_get_by_id:buildZip
#./gradlew :lambda_user_get_by_email:buildZip
#./gradlew :lambda_user_search:buildZip
#./gradlew :lambda_user_get_followers:buildZip
#./gradlew :lambda_user_get_number_of_followers:buildZip
#./gradlew :lambda_user_get_following:buildZip
#./gradlew :lambda_user_get_number_of_following:buildZip
#./gradlew :lambda_user_get_users_who_liked_post:buildZip
#
#./gradlew :lambda_follow_get_all:buildZip
#./gradlew :lambda_follow_save:buildZip
#./gradlew :lambda_follow_delete:buildZip
#./gradlew :lambda_follow_get_by_id:buildZip
#
#./gradlew :lambda_post_get_all:buildZip
#./gradlew :lambda_post_save:buildZip
#./gradlew :lambda_post_delete:buildZip
#./gradlew :lambda_post_edit_description:buildZip
#./gradlew :lambda_post_get_by_id:buildZip
#./gradlew :lambda_post_get_posts_of_user:buildZip
#./gradlew :lambda_post_get_number_of_posts_of_user:buildZip
#
#./gradlew :lambda_like_get_all:buildZip
#./gradlew :lambda_like_save:buildZip
#./gradlew :lambda_like_delete:buildZip
#./gradlew :lambda_like_get_by_id:buildZip
#./gradlew :lambda_like_get_likes_on_post:buildZip
#./gradlew :lambda_like_get_number_of_likes_on_post:buildZip
#
#./gradlew :lambda_comment_get_all:buildZip
#./gradlew :lambda_comment_save:buildZip
#./gradlew :lambda_comment_delete:buildZip
#./gradlew :lambda_comment_edit_text:buildZip
#./gradlew :lambda_comment_get_by_id:buildZip
#./gradlew :lambda_comment_on_post:buildZip
#
