package zlack.bra.instamini.test;

import com.amazonaws.services.lambda.runtime.*;

import java.util.HashMap;
import java.util.Map;

public class TestContext implements Context {

    @Override
    public String getAwsRequestId() {
        return "test";
    }

    @Override
    public String getLogGroupName() {
        return "test";
    }

    @Override
    public String getLogStreamName() {
        return "test";
    }

    @Override
    public String getFunctionName() {
        return "test";
    }

    @Override
    public String getFunctionVersion() {
        return "test";
    }

    @Override
    public String getInvokedFunctionArn() {
        return "test";
    }

    @Override
    public CognitoIdentity getIdentity() {
        return new CognitoIdentity() {
            @Override
            public String getIdentityId() {
                return "test";
            }

            @Override
            public String getIdentityPoolId() {
                return "test";
            }
        };
    }

    @Override
    public ClientContext getClientContext() {
        return new ClientContext() {
            @Override
            public Client getClient() {
                return new Client() {
                    @Override
                    public String getInstallationId() {
                        return "test";
                    }

                    @Override
                    public String getAppTitle() {
                        return "test";
                    }

                    @Override
                    public String getAppVersionName() {
                        return "test";
                    }

                    @Override
                    public String getAppVersionCode() {
                        return "test";
                    }

                    @Override
                    public String getAppPackageName() {
                        return "test";
                    }
                };
            }

            @Override
            public Map<String, String> getCustom() {
                return new HashMap<>();
            }

            @Override
            public Map<String, String> getEnvironment() {
                return new HashMap<>();
            }
        };
    }

    @Override
    public int getRemainingTimeInMillis() {
        return 0;
    }

    @Override
    public int getMemoryLimitInMB() {
        return 0;
    }

    @Override
    public LambdaLogger getLogger() {
        return new LambdaLogger() {
            @Override
            public void log(String message) {
            }

            @Override
            public void log(byte[] message) {
            }
        };
    }
}