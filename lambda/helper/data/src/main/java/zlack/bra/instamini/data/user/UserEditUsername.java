package zlack.bra.instamini.data.user;

public final class UserEditUsername {

    private String username;

    public UserEditUsername() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
