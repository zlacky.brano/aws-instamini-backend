package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertComments;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.*;

@Testcontainers
public class DeleteCommentHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private DeleteCommentHandler deleteCommentHandler;

    private final Comment commentToDelete = new Comment("id", "test", "test", "test", "test");

    private final Comment comment = new Comment("not", "not", "not", "not", "not");

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        deleteCommentHandler = new DeleteCommentHandler();

        Item commentToDeleteItem = CreateItem.createCommentItem(commentToDelete);
        Item commentItem = CreateItem.createCommentItem(comment);

        Table table = CreateTable.create(dynamoDB, TableName.commentTable);
        table.putItem(commentToDeleteItem);
        table.putItem(commentItem);
    }

    @Test
    public void handleRequest() {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", commentToDelete.getId());
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters);
        APIGatewayProxyResponseEvent response = deleteCommentHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkDeleted();
    }

    private void checkDeleted() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.commentTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Comment> comments = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Comment comment = new Comment(item.get("id").getS(), item.get("text").getS(), item.get("time").getS(), item.get("userId").getS(), item.get("postId").getS());
            comments.add(comment);
        }

        Assertions.assertEquals(1, comments.size());
        if (!AssertComments.commentsEquals(comment, comments.get(0))) {
            throw new AssertionFailedError();
        }
    }
}