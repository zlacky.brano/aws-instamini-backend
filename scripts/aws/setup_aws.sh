#!/bin/bash

echo; echo "----- Creating tables in DynamoDB -----"
bash ./scripts/aws/dynamodb/create_tables.sh

echo; echo "----- Creating Lambdas -----"
bash ./scripts/aws/lambda/create_lambdas.sh

echo; echo "----- Creating API Gateways -----"
bash ./scripts/aws/apigateway/main/create_api_and_resources.sh

echo; echo "Done"