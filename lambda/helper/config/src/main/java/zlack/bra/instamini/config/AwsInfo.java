package zlack.bra.instamini.config;

public class AwsInfo {

    public static final String serviceEndpoint = "http://localstack:4566";
    public static final String region = "eu-central-1";
}
