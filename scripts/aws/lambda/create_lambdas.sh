#!/bin/bash

echo "--- User ---"

echo " - Creating lambda GetUsersFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetUsersFunction --runtime java11 --handler zlack.bra.instamini.GetUsersHandler::handleRequest --zip-file fileb:///tmp/lambda/user/get_all/build/distributions/lambda_user_get_all-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda SaveUserFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name SaveUserFunction --runtime java11 --handler zlack.bra.instamini.SaveUserHandler::handleRequest --zip-file fileb:///tmp/lambda/user/save/build/distributions/lambda_user_save-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda DeleteUserFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name DeleteUserFunction --runtime java11 --handler zlack.bra.instamini.DeleteUserHandler::handleRequest --zip-file fileb:///tmp/lambda/user/delete/build/distributions/lambda_user_delete-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 180 1>/dev/null

echo " - Creating lambda EditUsernameFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name EditUsernameFunction --runtime java11 --handler zlack.bra.instamini.EditUsernameHandler::handleRequest --zip-file fileb:///tmp/lambda/user/edit_username/build/distributions/lambda_user_edit_username-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetUserByIdFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetUserByIdFunction --runtime java11 --handler zlack.bra.instamini.GetUserByIdHandler::handleRequest --zip-file fileb:///tmp/lambda/user/get_by_id/build/distributions/lambda_user_get_by_id-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetUserByEmailFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetUserByEmailFunction --runtime java11 --handler zlack.bra.instamini.GetUserByEmailHandler::handleRequest --zip-file fileb:///tmp/lambda/user/get_by_email/build/distributions/lambda_user_get_by_email-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda SearchUsersFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name SearchUsersFunction --runtime java11 --handler zlack.bra.instamini.SearchUsersHandler::handleRequest --zip-file fileb:///tmp/lambda/user/search/build/distributions/lambda_user_search-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetNumberOfFollowersFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetNumberOfFollowersFunction --runtime java11 --handler zlack.bra.instamini.GetNumberOfFollowersHandler::handleRequest --zip-file fileb:///tmp/lambda/user/get_number_of_followers/build/distributions/lambda_user_get_number_of_followers-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetFollowersFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetFollowersFunction --runtime java11 --handler zlack.bra.instamini.GetFollowersHandler::handleRequest --zip-file fileb:///tmp/lambda/user/get_followers/build/distributions/lambda_user_get_followers-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetNumberOfFollowingFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetNumberOfFollowingFunction --runtime java11 --handler zlack.bra.instamini.GetNumberOfFollowingHandler::handleRequest --zip-file fileb:///tmp/lambda/user/get_number_of_following/build/distributions/lambda_user_get_number_of_following-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetFollowingFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetFollowingFunction --runtime java11 --handler zlack.bra.instamini.GetFollowingHandler::handleRequest --zip-file fileb:///tmp/lambda/user/get_following/build/distributions/lambda_user_get_following-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetUsersWhoLikedPostFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetUsersWhoLikedPostFunction --runtime java11 --handler zlack.bra.instamini.GetUsersWhoLikedPostHandler::handleRequest --zip-file fileb:///tmp/lambda/user/get_users_who_liked_post/build/distributions/lambda_user_get_users_who_liked_post-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null


echo "--- Follow ---"

echo " - Creating lambda GetFollowsFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetFollowsFunction --runtime java11 --handler zlack.bra.instamini.GetFollowsHandler::handleRequest --zip-file fileb:///tmp/lambda/follow/get_all/build/distributions/lambda_follow_get_all-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda SaveFollowFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name SaveFollowFunction --runtime java11 --handler zlack.bra.instamini.SaveFollowHandler::handleRequest --zip-file fileb:///tmp/lambda/follow/save/build/distributions/lambda_follow_save-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda DeleteFollowFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name DeleteFollowFunction --runtime java11 --handler zlack.bra.instamini.DeleteFollowHandler::handleRequest --zip-file fileb:///tmp/lambda/follow/delete/build/distributions/lambda_follow_delete-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetFollowByIdFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetFollowByIdFunction --runtime java11 --handler zlack.bra.instamini.GetFollowByIdHandler::handleRequest --zip-file fileb:///tmp/lambda/follow/get_by_id/build/distributions/lambda_follow_get_by_id-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null


echo "--- Post ---"

echo " - Creating lambda GetPostsFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetPostsFunction --runtime java11 --handler zlack.bra.instamini.GetPostsHandler::handleRequest --zip-file fileb:///tmp/lambda/post/get_all/build/distributions/lambda_post_get_all-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda SavePostFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name SavePostFunction --runtime java11 --handler zlack.bra.instamini.SavePostHandler::handleRequest --zip-file fileb:///tmp/lambda/post/save/build/distributions/lambda_post_save-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda DeletePostFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name DeletePostFunction --runtime java11 --handler zlack.bra.instamini.DeletePostHandler::handleRequest --zip-file fileb:///tmp/lambda/post/delete/build/distributions/lambda_post_delete-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 180 1>/dev/null

echo " - Creating lambda EditDescriptionFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name EditDescriptionFunction --runtime java11 --handler zlack.bra.instamini.EditDescriptionHandler::handleRequest --zip-file fileb:///tmp/lambda/post/edit_description/build/distributions/lambda_post_edit_description-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetPostByIdFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetPostByIdFunction --runtime java11 --handler zlack.bra.instamini.GetPostByIdHandler::handleRequest --zip-file fileb:///tmp/lambda/post/get_by_id/build/distributions/lambda_post_get_by_id-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetNumberOfPostsOfUserFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetNumberOfPostsOfUserFunction --runtime java11 --handler zlack.bra.instamini.GetNumberOfPostsOfUserHandler::handleRequest --zip-file fileb:///tmp/lambda/post/get_number_of_posts_of_user/build/distributions/lambda_post_get_number_of_posts_of_user-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetPostsOfUserFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetPostsOfUserFunction --runtime java11 --handler zlack.bra.instamini.GetPostsOfUserHandler::handleRequest --zip-file fileb:///tmp/lambda/post/get_posts_of_user/build/distributions/lambda_post_get_posts_of_user-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null


echo "--- Like ---"

echo " - Creating lambda GetLikesFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetLikesFunction --runtime java11 --handler zlack.bra.instamini.GetLikesHandler::handleRequest --zip-file fileb:///tmp/lambda/like/get_all/build/distributions/lambda_like_get_all-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda SaveLikeFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name SaveLikeFunction --runtime java11 --handler zlack.bra.instamini.SaveLikeHandler::handleRequest --zip-file fileb:///tmp/lambda/like/save/build/distributions/lambda_like_save-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda DeleteLikeFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name DeleteLikeFunction --runtime java11 --handler zlack.bra.instamini.DeleteLikeHandler::handleRequest --zip-file fileb:///tmp/lambda/like/delete/build/distributions/lambda_like_delete-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetLikeByIdFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetLikeByIdFunction --runtime java11 --handler zlack.bra.instamini.GetLikeByIdHandler::handleRequest --zip-file fileb:///tmp/lambda/like/get_by_id/build/distributions/lambda_like_get_by_id-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetNumberOfLikesOnPostFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetNumberOfLikesOnPostFunction --runtime java11 --handler zlack.bra.instamini.GetNumberOfLikesOnPostHandler::handleRequest --zip-file fileb:///tmp/lambda/like/get_number_of_likes_on_post/build/distributions/lambda_like_get_number_of_likes_on_post-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetLikesOnPostFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetLikesOnPostFunction --runtime java11 --handler zlack.bra.instamini.GetLikesOnPostHandler::handleRequest --zip-file fileb:///tmp/lambda/like/get_likes_on_post/build/distributions/lambda_like_get_likes_on_post-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null


echo "--- Comment ---"

echo " - Creating lambda GetCommentsFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetCommentsFunction --runtime java11 --handler zlack.bra.instamini.GetCommentsHandler::handleRequest --zip-file fileb:///tmp/lambda/comment/get_all/build/distributions/lambda_comment_get_all-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda SaveCommentFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name SaveCommentFunction --runtime java11 --handler zlack.bra.instamini.SaveCommentHandler::handleRequest --zip-file fileb:///tmp/lambda/comment/save/build/distributions/lambda_comment_save-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda DeleteCommentFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name DeleteCommentFunction --runtime java11 --handler zlack.bra.instamini.DeleteCommentHandler::handleRequest --zip-file fileb:///tmp/lambda/comment/delete/build/distributions/lambda_comment_delete-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda EditTextFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name EditTextFunction --runtime java11 --handler zlack.bra.instamini.EditTextHandler::handleRequest --zip-file fileb:///tmp/lambda/comment/edit_text/build/distributions/lambda_comment_edit_text-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda GetCommentByIdFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name GetCommentByIdFunction --runtime java11 --handler zlack.bra.instamini.GetCommentByIdHandler::handleRequest --zip-file fileb:///tmp/lambda/comment/get_by_id/build/distributions/lambda_comment_get_by_id-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null

echo " - Creating lambda CommentsOnPostFunction"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 lambda create-function --function-name CommentsOnPostFunction --runtime java11 --handler zlack.bra.instamini.CommentsOnPostHandler::handleRequest --zip-file fileb:///tmp/lambda/comment/on_post/build/distributions/lambda_comment_on_post-1.0-SNAPSHOT.zip --role arn:aws:iam::000000000:role/dummy --timeout 60 1>/dev/null
