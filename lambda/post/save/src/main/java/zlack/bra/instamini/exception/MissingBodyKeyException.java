package zlack.bra.instamini.exception;

public class MissingBodyKeyException extends Exception {

    public MissingBodyKeyException(String errorMessage) {
        super(errorMessage);
    }
}
