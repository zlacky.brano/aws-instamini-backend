package zlack.bra.instamini.data.user;

public final class UserCreate {

    private String username;
    private String email;

    public UserCreate() {
    }

    public UserCreate(String username, String email) {
        this.username = username;
        this.email = email;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String toJson() {
        return "{\"username\":\"" + username + "\", \"email\":\"" + email + "\"}";
    }
}
