#!/bin/bash

# First argument - RestAPI ID
# Second argument - Region
# Third argument - Stage name

LIKES_PATH="likes"
LIKES_ID_PARAMETER="id"
LIKES_COUNT_PATH="count"
LIKES_ON_POST_PATH="onPost"

echo "   - Creating resource for path /${LIKES_PATH}, /${LIKES_PATH}/{${LIKES_ID_PARAMETER}}, /${LIKES_PATH}/${LIKES_COUNT_PATH}, /${LIKES_PATH}/${LIKES_ON_POST_PATH}"
LIKES_RESOURCE_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${LIKES_PATH}")
RESOURCE_ID_WITH_PARAM_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "{${LIKES_ID_PARAMETER}}" "${LIKES_RESOURCE_ID}")
RESOURCE_COUNT_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${LIKES_COUNT_PATH}" "${LIKES_RESOURCE_ID}")
RESOURCE_ON_POST_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${LIKES_ON_POST_PATH}" "${LIKES_RESOURCE_ID}")

echo "    - Creating method and integration for GetLikesResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${LIKES_RESOURCE_ID}" "GET" "GetLikesFunction"

echo "    - Creating method and integration for SaveLikeResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${LIKES_RESOURCE_ID}" "POST" "SaveLikeFunction"

echo "    - Creating method and integration for DeleteLikeResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "DELETE" "DeleteLikeFunction" "${LIKES_ID_PARAMETER}"

echo "    - Creating method and integration for GetLikeByIdResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "GET" "GetLikeByIdFunction" "${LIKES_ID_PARAMETER}"

echo "    - Creating method and integration for GetNumberOfLikesOnPostResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_COUNT_ID}" "GET" "GetNumberOfLikesOnPostFunction"

echo "    - Creating method and integration for GetLikesOnPostResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ON_POST_ID}" "GET" "GetLikesOnPostFunction"

echo "   - Base URL: http://localhost:4566/restapis/$1/$3/_user_request_/${LIKES_PATH}"