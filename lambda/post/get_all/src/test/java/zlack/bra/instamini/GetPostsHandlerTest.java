package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertPosts;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.List;


@Testcontainers
class GetPostsHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetPostsHandler getPostsHandler;

    private final List<Post> posts = new ArrayList<>() {
        {
            add(new Post("id", "test", "test", "test", "test"));
            add(new Post("not", "not", "not", "not", "not"));
            add(new Post("1", "2", "3", "4", "5"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getPostsHandler = new GetPostsHandler();

        Table table = CreateTable.create(dynamoDB, TableName.postTable);

        for (Post post : posts) {
            table.putItem(CreateItem.createPostItem(post));
        }
    }

    @Test
    void handleRequest() throws JsonProcessingException {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent();
        APIGatewayProxyResponseEvent response = getPostsHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkPosts(response.getBody());
    }

    private void checkPosts(String bodyResult) throws JsonProcessingException {
        Post[] postsResult = new ObjectMapper().readValue(bodyResult, Post[].class);

        Assertions.assertEquals(3, postsResult.length);
        boolean found;
        for (Post postResult : postsResult) {
            found = false;
            for (Post post : posts) {
                if (post.getId().equals(postResult.getId())) {
                    found = true;
                    if (!AssertPosts.postsEquals(postResult, post)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }
}