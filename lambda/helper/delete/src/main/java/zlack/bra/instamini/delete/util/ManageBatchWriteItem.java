package zlack.bra.instamini.delete.util;

import com.amazonaws.services.dynamodbv2.document.BatchWriteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.TableWriteItems;
import com.amazonaws.services.dynamodbv2.model.WriteRequest;
import zlack.bra.instamini.instance.InstanceModule;

import java.util.List;
import java.util.Map;

public final class ManageBatchWriteItem {

    private static final DynamoDB dynamoDB = InstanceModule.dynamoDB;

    public static void manage(TableWriteItems writeItems) {
        BatchWriteItemOutcome outcome = dynamoDB.batchWriteItem(writeItems);

        do {
            Map<String, List<WriteRequest>> unprocessedItems = outcome.getUnprocessedItems();

            if (!outcome.getUnprocessedItems().isEmpty()) {
                outcome = dynamoDB.batchWriteItemUnprocessed(unprocessedItems);
            }
        } while (!outcome.getUnprocessedItems().isEmpty());
    }
}
