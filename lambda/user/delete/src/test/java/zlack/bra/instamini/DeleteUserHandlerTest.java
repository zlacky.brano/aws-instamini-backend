package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.data.follow.Follow;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.InvokeLambda;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.*;
import zlack.bra.instamini.test.item.CreateItem;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
public class DeleteUserHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private DeleteUserHandler deleteUserHandler;

    private ObjectMapper objectMapper = InstanceModule.objectMapper;

    private final User userToDelete = new User("id", "test", "test");

    private final User user = new User("not", "not", "not");

    private final Post post = new Post("not", "not", "not", "not", "not");

    private final List<Post> postsToDelete = new ArrayList<>() {
        {
            add(new Post("id", "test", "test", "test", "id"));
            add(new Post("id2", "test", "test", "test", "id"));
        }
    };

    private final List<Comment> commentsToDelete = new ArrayList<>() {
        {
            add(new Comment("idComment", "test", "test", "id", "id"));
            add(new Comment("a", "a", "a", "not", "id2"));
            add(new Comment("b", "a", "a", "id", "not"));
        }
    };

    private final List<Comment> comments = new ArrayList<>() {
        {
            add(new Comment("idComment2", "test", "test", "not", "not"));
            add(new Comment("a2", "a", "a", "not", "not"));
        }
    };

    private final List<Follow> followsToDelete = new ArrayList<>() {
        {
            add(new Follow("id1", "id", "not"));
            add(new Follow("id2", "not", "id"));
        }
    };

    private final List<Follow> follows = new ArrayList<>() {
        {
            add(new Follow("id3", "not", "not"));
            add(new Follow("id4", "not", "not"));
        }
    };

    private final List<Like> likesToDelete = new ArrayList<>() {
        {
            add(new Like("idLike", "id", "id"));
            add(new Like("a", "not", "id2"));
            add(new Like("b", "id", "not"));
        }
    };

    private final List<Like> likes = new ArrayList<>() {
        {
            add(new Like("idLike2", "not", "not"));
            add(new Like("a2", "not", "not"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        deleteUserHandler = new DeleteUserHandler();

        Item userToDeleteItem = CreateItem.createUserItem(userToDelete);
        Item userItem = CreateItem.createUserItem(user);

        Table userTable = CreateTable.create(dynamoDB, TableName.userTable);
        userTable.putItem(userToDeleteItem);
        userTable.putItem(userItem);

        Table followTable = CreateTable.create(dynamoDB, TableName.followTable);
        Table postTable = CreateTable.create(dynamoDB, TableName.postTable);
        Table commentTable = CreateTable.create(dynamoDB, TableName.commentTable);
        Table likeTable = CreateTable.create(dynamoDB, TableName.likeTable);

        postTable.putItem(CreateItem.createPostItem(post));

        for (Post post: postsToDelete) {
            postTable.putItem(CreateItem.createPostItem(post));
        }
        for (Comment comment: commentsToDelete) {
            commentTable.putItem(CreateItem.createCommentItem(comment));
        }
        for (Comment comment: comments) {
            commentTable.putItem(CreateItem.createCommentItem(comment));
        }
        for (Follow follow: followsToDelete) {
            followTable.putItem(CreateItem.createFollowItem(follow));
        }
        for (Follow follow: follows) {
            followTable.putItem(CreateItem.createFollowItem(follow));
        }
        for (Like like: likesToDelete) {
            likeTable.putItem(CreateItem.createLikeItem(like));
        }
        for (Like like: likes) {
            likeTable.putItem(CreateItem.createLikeItem(like));
        }
    }

    @Test
    public void handleRequest() throws JsonProcessingException {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", userToDelete.getId());
            }
        };

        String commentsJsonPostDelete0 = objectMapper.writeValueAsString(List.of());
        commentsJsonPostDelete0 = "{\"body\":[" + commentsJsonPostDelete0 + "], \"statusCode\":200}";
        InvokeResult commentsOnPostDelete0Result = new InvokeResult().withPayload(ByteBuffer.wrap(commentsJsonPostDelete0.getBytes()));

        String commentsJsonPostDelete1 = objectMapper.writeValueAsString(commentsToDelete.subList(1, 2));
        commentsJsonPostDelete1 = "{\"body\":[" + commentsJsonPostDelete1 + "], \"statusCode\":200}";
        InvokeResult commentsOnPostDelete1Result = new InvokeResult().withPayload(ByteBuffer.wrap(commentsJsonPostDelete1.getBytes()));

        String likesJsonDelete0 = objectMapper.writeValueAsString(List.of());
        likesJsonDelete0 = "{\"body\":[" + likesJsonDelete0 + "], \"statusCode\":200}";
        InvokeResult likesOnPostDelete0Result = new InvokeResult().withPayload(ByteBuffer.wrap(likesJsonDelete0.getBytes()));

        String likesJsonDelete1 = objectMapper.writeValueAsString(likesToDelete.subList(1, 2));
        likesJsonDelete1 = "{\"body\":[" + likesJsonDelete1 + "], \"statusCode\":200}";
        InvokeResult likesOnPostDelete1Result = new InvokeResult().withPayload(ByteBuffer.wrap(likesJsonDelete1.getBytes()));

        String postsJson = objectMapper.writeValueAsString(postsToDelete);
        postsJson = "{\"body\":[" + postsJson + "], \"statusCode\":200}";
        InvokeResult postsOfUserResult = new InvokeResult().withPayload(ByteBuffer.wrap(postsJson.getBytes()));

        try (MockedStatic<InvokeLambda> utilities = Mockito.mockStatic(InvokeLambda.class)) {
            utilities.when(() -> InvokeLambda.invokeWithQueryParam("CommentsOnPostFunction", "postId", postsToDelete.get(0).getId()))
                    .thenReturn(commentsOnPostDelete0Result);
            utilities.when(() -> InvokeLambda.invokeWithQueryParam("GetLikesOnPostFunction", "postId", postsToDelete.get(0).getId()))
                    .thenReturn(likesOnPostDelete0Result);
            utilities.when(() -> InvokeLambda.invokeWithQueryParam("CommentsOnPostFunction", "postId", postsToDelete.get(1).getId()))
                    .thenReturn(commentsOnPostDelete1Result);
            utilities.when(() -> InvokeLambda.invokeWithQueryParam("GetLikesOnPostFunction", "postId", postsToDelete.get(1).getId()))
                    .thenReturn(likesOnPostDelete1Result);
            utilities.when(() -> InvokeLambda.invokeWithQueryParam("GetPostsOfUserFunction", "userId", userToDelete.getId()))
                    .thenReturn(postsOfUserResult);

            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withPathParameters(pathParameters);
            APIGatewayProxyResponseEvent response = deleteUserHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(200, response.getStatusCode());

            utilities.verify(() -> InvokeLambda.invokeWithQueryParam("CommentsOnPostFunction", "postId", postsToDelete.get(0).getId()));
            utilities.verify(() ->  InvokeLambda.invokeWithQueryParam("GetLikesOnPostFunction", "postId", postsToDelete.get(0).getId()));
            utilities.verify(() -> InvokeLambda.invokeWithQueryParam("CommentsOnPostFunction", "postId", postsToDelete.get(1).getId()));
            utilities.verify(() ->  InvokeLambda.invokeWithQueryParam("GetLikesOnPostFunction", "postId", postsToDelete.get(1).getId()));
            utilities.verify(() -> InvokeLambda.invokeWithQueryParam("GetPostsOfUserFunction", "userId", userToDelete.getId()));
        }

        checkDeletedUsers();
        checkDeletedPosts();
        checkDeletedComments();
        checkDeletedLikes();
        checkDeletedFollows();
    }

    private void checkDeletedUsers() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.userTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<User> usersScan = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            User user = new User(item.get("id").getS(), item.get("username").getS(), item.get("email").getS());
            usersScan.add(user);
        }

        Assertions.assertEquals(1, usersScan.size());
        if (!AssertUsers.usersEquals(user, usersScan.get(0))) {
            throw new AssertionFailedError();
        }
    }

    private void checkDeletedPosts() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.postTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Post> postsScan = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Post post = new Post(item.get("id").getS(), item.get("description").getS(), item.get("photoUrl").getS(), item.get("time").getS(), item.get("userId").getS());
            postsScan.add(post);
        }

        Assertions.assertEquals(1, postsScan.size());
        if (!AssertPosts.postsEquals(post, postsScan.get(0))) {
            throw new AssertionFailedError();
        }
    }

    private void checkDeletedComments() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.commentTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Comment> commentsScan = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Comment comment = new Comment(item.get("id").getS(), item.get("text").getS(), item.get("time").getS(), item.get("userId").getS(), item.get("postId").getS());
            commentsScan.add(comment);
        }

        Assertions.assertEquals(2, commentsScan.size());
        boolean found;
        for (Comment commentScan : commentsScan) {
            found = false;
            for (Comment comment : comments) {
                if (comment.getId().equals(commentScan.getId())) {
                    found = true;
                    if (!AssertComments.commentsEquals(commentScan, comment)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }

    private void checkDeletedLikes() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.likeTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Like> likesScan = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Like like = new Like(item.get("id").getS(), item.get("userId").getS(), item.get("postId").getS());
            likesScan.add(like);
        }

        Assertions.assertEquals(2, likesScan.size());
        boolean found;
        for (Like likeScan : likesScan) {
            found = false;
            for (Like like : likes) {
                if (like.getId().equals(likeScan.getId())) {
                    found = true;
                    if (!AssertLikes.likesEquals(likeScan, like)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }

    private void checkDeletedFollows() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.followTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Follow> followsScan = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Follow follow = new Follow(item.get("id").getS(), item.get("userIdWho").getS(), item.get("userIdWhom").getS());
            followsScan.add(follow);
        }

        Assertions.assertEquals(2, followsScan.size());
        boolean found;
        for (Follow followScan : followsScan) {
            found = false;
            for (Follow follow : follows) {
                if (follow.getId().equals(followScan.getId())) {
                    found = true;
                    if (!AssertFollows.followsEquals(followScan, follow)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }
}