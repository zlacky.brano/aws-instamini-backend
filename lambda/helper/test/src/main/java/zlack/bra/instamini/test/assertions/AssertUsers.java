package zlack.bra.instamini.test.assertions;

import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.data.user.UserCreate;

public class AssertUsers {

    public static boolean usersEquals(User user1, User user2) {
        return user1.getId().equals(user2.getId()) &&
                user1.getUsername().equals(user2.getUsername()) &&
                user1.getEmail().equals(user2.getEmail());
    }

    public static boolean usersEquals(User user1, User user2, String newUsername) {
        return user1.getId().equals(user2.getId()) &&
                newUsername.equals(user2.getUsername()) &&
                user1.getEmail().equals(user2.getEmail());
    }

    public static boolean usersEquals(UserCreate userCreate, User user) {
        return !user.getId().isBlank() &&
                userCreate.getUsername().equals(user.getUsername()) &&
                userCreate.getEmail().equals(user.getEmail());
    }
}
