package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertLikes;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
public class DeleteLikeHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private DeleteLikeHandler deleteLikeHandler;

    private final Like likeToDelete = new Like("id", "test", "test");

    private final Like like = new Like("not", "not", "not");

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        deleteLikeHandler = new DeleteLikeHandler();

        Item likeToDeleteItem = CreateItem.createLikeItem(likeToDelete);
        Item likeItem = CreateItem.createLikeItem(like);

        Table table = CreateTable.create(dynamoDB, TableName.likeTable);
        table.putItem(likeToDeleteItem);
        table.putItem(likeItem);
    }

    @Test
    public void handleRequest() {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", likeToDelete.getId());
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters);
        APIGatewayProxyResponseEvent response = deleteLikeHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkDeleted();
    }

    private void checkDeleted() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.likeTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Like> likes = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Like like = new Like(item.get("id").getS(), item.get("userId").getS(), item.get("postId").getS());
            likes.add(like);
        }

        Assertions.assertEquals(1, likes.size());
        if (!AssertLikes.likesEquals(like, likes.get(0))) {
            throw new AssertionFailedError();
        }
    }
}