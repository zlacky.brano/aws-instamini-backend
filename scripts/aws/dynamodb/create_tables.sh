#!/bin/bash

echo " - Creating table User"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 dynamodb create-table --table-name User --attribute-definitions AttributeName=id,AttributeType=S --key-schema AttributeName=id,KeyType=HASH --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --table-class STANDARD 1>/dev/null

echo " - Creating table Follow"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 dynamodb create-table --table-name Follow --attribute-definitions AttributeName=id,AttributeType=S --key-schema AttributeName=id,KeyType=HASH --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --table-class STANDARD 1>/dev/null

echo " - Creating table Post"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 dynamodb create-table --table-name Post --attribute-definitions AttributeName=id,AttributeType=S --key-schema AttributeName=id,KeyType=HASH --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --table-class STANDARD 1>/dev/null

echo " - Creating table Like"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 dynamodb create-table --table-name Like --attribute-definitions AttributeName=id,AttributeType=S --key-schema AttributeName=id,KeyType=HASH --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --table-class STANDARD 1>/dev/null

echo " - Creating table Comment"
docker-compose run --rm aws-cli --endpoint-url http://localstack:4566 dynamodb create-table --table-name Comment --attribute-definitions AttributeName=id,AttributeType=S --key-schema AttributeName=id,KeyType=HASH --provisioned-throughput ReadCapacityUnits=10,WriteCapacityUnits=5 --table-class STANDARD 1>/dev/null
