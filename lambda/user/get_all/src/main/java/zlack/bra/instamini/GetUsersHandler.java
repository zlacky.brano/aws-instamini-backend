package zlack.bra.instamini;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;


public class GetUsersHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;
    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    /**
     * Gets all users
     * @param event all information about request
     * @param context request context
     * @return 200, if it was successful
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            ScanRequest scanRequest = new ScanRequest()
                    .withTableName(TableName.userTable);

            ScanResult scanResult = client.scan(scanRequest);
            List<User> users = new ArrayList<>();
            for (Map<String, AttributeValue> item : scanResult.getItems()) {
                User user = new User(item.get("id").getS(), item.get("username").getS(), item.get("email").getS());
                users.add(user);
            }

            final String result = objectMapper.writeValueAsString(users);
            return response.withStatusCode(200).withBody(result);
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
