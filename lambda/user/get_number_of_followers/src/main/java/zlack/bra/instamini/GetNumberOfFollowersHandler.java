package zlack.bra.instamini;


import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.dynamodbv2.model.Select;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.instance.InstanceModule;

import static java.util.Map.of;


public class GetNumberOfFollowersHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;

    /**
     * Gets number of all followers
     * @param event all information about request
     * @param context request context
     * @return 200, if it was successful or 400, if query param userId is missing
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            Map<String, String> queryParameters = event.getQueryStringParameters();
            if (queryParameters.isEmpty() || !queryParameters.containsKey("userId")) {
                return response.withStatusCode(400).withBody("{\"errorMessage\":\"Missing query parameter userId\"}");
            }

            ScanRequest scanRequest = new ScanRequest()
                    .withTableName(TableName.followTable)
                    .withSelect(Select.COUNT)
                    .withFilterExpression("#uWhom = :userId")
                    .withExpressionAttributeValues(of(
                            ":userId", new AttributeValue(queryParameters.get("userId"))
                    ))
                    .withExpressionAttributeNames(of("#uWhom", "userIdWhom"));

            ScanResult scanResult = client.scan(scanRequest);

            return response.withStatusCode(200).withBody("{\"result\":" + scanResult.getCount() + "}");
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
