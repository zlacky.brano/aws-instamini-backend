package zlack.bra.instamini.config;

public class TableName {

    public static final String userTable = "User";
    public static final String postTable = "Post";
    public static final String commentTable = "Comment";
    public static final String likeTable = "Like";
    public static final String followTable = "Follow";
}
