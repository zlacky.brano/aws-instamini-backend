package zlack.bra.instamini.data.post;

import java.io.InputStream;

public final class PostCreate {

    private String description;

    private String userId;

    private byte[] image;

    public PostCreate() {
    }

    public PostCreate(String description, String userId, byte[] image) {
        this.description = description;
        this.userId = userId;
        this.image = image;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public byte[] getImage() {
        return image;
    }

    public void setImage(byte[] image) {
        this.image = image;
    }
}
