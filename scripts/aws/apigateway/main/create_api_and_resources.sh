#!/bin/bash

REST_API_NAME="InstaminiRestAPI"
STAGE_NAME="test"
REGION="eu-central-1"

echo " - Creating InstaminiRestAPI"
REST_API_ID=$(docker-compose run --rm aws-cli apigateway create-rest-api --region "${REGION}" --name "${REST_API_NAME}" --endpoint-url=http://localstack:4566 | jq '.id' | tr -d '"')

echo "  - Creating User's resources"
bash ./scripts/aws/apigateway/particular/user/create_user_resources.sh "${REST_API_ID}" "${REGION}" "${STAGE_NAME}"

echo "  - Creating Follow's resources"
bash ./scripts/aws/apigateway/particular/follow/create_follow_resources.sh "${REST_API_ID}" "${REGION}" "${STAGE_NAME}"

echo "  - Creating Post's resources"
bash ./scripts/aws/apigateway/particular/post/create_post_resources.sh "${REST_API_ID}" "${REGION}" "${STAGE_NAME}"

echo "  - Creating Like's resources"
bash ./scripts/aws/apigateway/particular/like/create_like_resources.sh "${REST_API_ID}" "${REGION}" "${STAGE_NAME}"

echo "  - Creating Comment's resources"
bash ./scripts/aws/apigateway/particular/comment/create_comment_resources.sh "${REST_API_ID}" "${REGION}" "${STAGE_NAME}"

echo "  - Deployment of InstaminiRestAPI"
docker-compose run --rm aws-cli apigateway create-deployment --region "${REGION}" --rest-api-id "${REST_API_ID}" --stage-name "${STAGE_NAME}" --endpoint-url=http://localstack:4566 1>/dev/null
