#!/bin/bash

# First argument - RestAPI ID
# Second argument - Region
# Third argument - Stage name

USERS_PATH="users"
USERS_ID_PARAMETER="id"
USERS_EDIT_USERNAME_PATH="editUsername"
USERS_BY_EMAIL="byEmail"
USERS_SEARCH="search"
USERS_FOLLOWERS_PATH="followers"
USERS_FOLLOWING_PATH="following"
USERS_COUNT="count"
USERS_LIKED_POST="likedPost"

echo "   - Creating resource for path /${USERS_PATH}, /${USERS_PATH}/{${USERS_ID_PARAMETER}}, /${USERS_PATH}/{${USERS_ID_PARAMETER}}/${USERS_EDIT_USERNAME_PATH}, /${USERS_PATH}/${USERS_BY_EMAIL}, /${USERS_PATH}/${USERS_SEARCH}, /${USERS_PATH}/${USERS_FOLLOWERS_PATH}, /${USERS_PATH}/${USERS_FOLLOWERS_PATH}/${USERS_COUNT}, /${USERS_PATH}/${USERS_LIKED_POST}"
USERS_RESOURCE_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${USERS_PATH}")
RESOURCE_ID_WITH_PARAM_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "{${USERS_ID_PARAMETER}}" "${USERS_RESOURCE_ID}")
RESOURCE_ID_WITH_PARAM_ID_EDIT_USERNAME=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${USERS_EDIT_USERNAME_PATH}" "${RESOURCE_ID_WITH_PARAM_ID}")
RESOURCE_BY_EMAIL=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${USERS_BY_EMAIL}" "${USERS_RESOURCE_ID}")
RESOURCE_SEARCH=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${USERS_SEARCH}" "${USERS_RESOURCE_ID}")
RESOURCE_FOLLOWERS=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${USERS_FOLLOWERS_PATH}" "${USERS_RESOURCE_ID}")
RESOURCE_COUNT_FOLLOWERS=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${USERS_COUNT}" "${RESOURCE_FOLLOWERS}")
RESOURCE_FOLLOWING=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${USERS_FOLLOWING_PATH}" "${USERS_RESOURCE_ID}")
RESOURCE_COUNT_FOLLOWING=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${USERS_COUNT}" "${RESOURCE_FOLLOWING}")
RESOURCE_LIKED_POST=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${USERS_LIKED_POST}" "${USERS_RESOURCE_ID}")

echo "    - Creating method and integration for GetUsersResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${USERS_RESOURCE_ID}" "GET" "GetUsersFunction"

echo "    - Creating method and integration for SaveUserResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${USERS_RESOURCE_ID}" "POST" "SaveUserFunction"

echo "    - Creating method and integration for DeleteUserResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "DELETE" "DeleteUserFunction" "${USERS_ID_PARAMETER}"

echo "    - Creating method and integration for EditUsernameResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID_EDIT_USERNAME}" "PUT" "EditUsernameFunction" "${USERS_ID_PARAMETER}"

echo "    - Creating method and integration for GetUserByIdResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "GET" "GetUserByIdFunction" "${USERS_ID_PARAMETER}"

echo "    - Creating method and integration for GetUserByEmailResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_BY_EMAIL}" "GET" "GetUserByEmailFunction"

echo "    - Creating method and integration for SearchUsersResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_SEARCH}" "GET" "SearchUsersFunction"

echo "    - Creating method and integration for GetFollowersResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_FOLLOWERS}" "GET" "GetFollowersFunction"

echo "    - Creating method and integration for GetNumberOfFollowersResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_COUNT_FOLLOWERS}" "GET" "GetNumberOfFollowersFunction"

echo "    - Creating method and integration for GetFollowingResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_FOLLOWING}" "GET" "GetFollowingFunction"

echo "    - Creating method and integration for GetNumberOfFollowingResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_COUNT_FOLLOWING}" "GET" "GetNumberOfFollowingFunction"

echo "    - Creating method and integration for GetUsersWhoLikedPostResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_LIKED_POST}" "GET" "GetUsersWhoLikedPostFunction"

echo "   - Base URL: http://localhost:4566/restapis/$1/$3/_user_request_/${USERS_PATH}"