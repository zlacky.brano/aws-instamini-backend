package zlack.bra.instamini.data.comment;

public final class CommentEditText {

    private String text;

    public CommentEditText() {
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
