package zlack.bra.instamini.data.post;

public final class Post {

    private String id;
    private String description;
    private String photoUrl;
    private String time;
    private String userId;

    public Post() {
    }

    public Post(String id, String description, String photoUrl, String time, String userId) {
        this.id = id;
        this.description = description;
        this.photoUrl = photoUrl;
        this.time = time;
        this.userId = userId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPhotoUrl() {
        return photoUrl;
    }

    public void setPhotoUrl(String photoUrl) {
        this.photoUrl = photoUrl;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }
}
