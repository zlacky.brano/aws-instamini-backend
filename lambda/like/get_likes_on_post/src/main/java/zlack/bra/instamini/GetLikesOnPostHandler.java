package zlack.bra.instamini;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.instance.InstanceModule;

import static java.util.Map.of;


public class GetLikesOnPostHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;
    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    /**
     * Gets likes on post
     * @param event all information about request
     * @param context request context
     * @return 200, if it was successful or 400, if query param postId is missing
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            Map<String, String> queryParameters = event.getQueryStringParameters();
            if (queryParameters.isEmpty() || !queryParameters.containsKey("postId")) {
                return response.withStatusCode(400).withBody("{\"errorMessage\":\"Missing query parameter postId\"}");
            }

            ScanRequest scanRequest = new ScanRequest()
                    .withTableName(TableName.likeTable)
                    .withFilterExpression("#p = :postId")
                    .withExpressionAttributeNames(of("#p", "postId"))
                    .withExpressionAttributeValues(of(
                            ":postId", new AttributeValue(queryParameters.get("postId"))
                    ));

            ScanResult scanResult = client.scan(scanRequest);
            List<Like> likes = new ArrayList<>();
            for (Map<String, AttributeValue> item : scanResult.getItems()) {
                Like like = new Like(item.get("id").getS(), item.get("userId").getS(), item.get("postId").getS());
                likes.add(like);
            }

            final String result = objectMapper.writeValueAsString(likes);
            return response.withStatusCode(200).withBody(result);
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
