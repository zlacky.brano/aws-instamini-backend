package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.like.LikeCreate;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.CheckIfDocumentIdExists;

import java.util.*;


public class SaveLikeHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;
    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    /**
     * Saves like
     * @param event all information about request
     * @param context request context
     * @return 201, if it was successful or 404, if user or post does not exist
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            LikeCreate like = objectMapper.readValue(event.getBody(), LikeCreate.class);

            InvokeResult userGetByIdResult = CheckIfDocumentIdExists.checkIfUserExists(like.getUserId());
            InvokeResult postGetByIdResult = CheckIfDocumentIdExists.checkIfPostExists(like.getPostId());
            if (userGetByIdResult.getFunctionError() != null || postGetByIdResult.getFunctionError() != null) {
                return response.withStatusCode(500).withBody("{}");
            }

            int statusCodeUser = objectMapper.readTree(new ByteBufferBackedInputStream(userGetByIdResult.getPayload())).get("statusCode").asInt();
            int statusCodePost = objectMapper.readTree(new ByteBufferBackedInputStream(postGetByIdResult.getPayload())).get("statusCode").asInt();
            if (statusCodeUser != 200) {
                return response.withStatusCode(statusCodeUser).withBody("{\"errorMessage\":\"User with id userId does not exist or there was a server error.\"}");
            } else if (statusCodePost != 200) {
                return response.withStatusCode(statusCodePost).withBody("{\"errorMessage\":\"Post with id postId does not exist or there was a server error.\"}");
            }

            Map<String, AttributeValue> likeMap = new HashMap<>();
            likeMap.put("id", new AttributeValue(UUID.randomUUID().toString()));
            likeMap.put("userId", new AttributeValue(like.getUserId()));
            likeMap.put("postId", new AttributeValue(like.getPostId()));

            PutItemRequest putItemRequest = new PutItemRequest()
                    .withTableName(TableName.likeTable).withItem(likeMap);

            client.putItem(putItemRequest);
            final String result = "{\"message\": \"Like created with id " + likeMap.get("id").getS() + "\"}";
            return response.withStatusCode(201).withBody(result);
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
