package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.follow.Follow;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertUsers;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
class GetFollowingHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetFollowingHandler getFollowingHandler;

    private final List<User> users = new ArrayList<>() {
        {
            add(new User("id", "test", "test"));
            add(new User("id2", "test2", "test2"));
            add(new User("not", "2", "3"));
        }
    };

    private final List<Follow> follows = new ArrayList<>() {
        {
            add(new Follow("id", "id", "id"));
            add(new Follow("id1", "id", "id2"));
            add(new Follow("id2", "id2", "id"));
            add(new Follow("id3", "id2", "not"));
            add(new Follow("id4", "not", "id2"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getFollowingHandler = new GetFollowingHandler();

        Table userTable = CreateTable.create(dynamoDB, TableName.userTable);
        Table followTable = CreateTable.create(dynamoDB, TableName.followTable);

        for (User user : users) {
            userTable.putItem(CreateItem.createUserItem(user));
        }
        for (Follow follow : follows) {
            followTable.putItem(CreateItem.createFollowItem(follow));
        }
    }

    @Test
    void handleRequest() throws JsonProcessingException {
        Map<String, String> queryParameters = new HashMap<>() {
            {
                put("userId", "id");
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(queryParameters);
        APIGatewayProxyResponseEvent response = getFollowingHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkUsers(response.getBody());
    }

    @Test
    void handleRequest400() {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(new HashMap<>());
        APIGatewayProxyResponseEvent response = getFollowingHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(400, response.getStatusCode());
    }

    private void checkUsers(String bodyResult) throws JsonProcessingException {
        User[] usersResult = new ObjectMapper().readValue(bodyResult, User[].class);

        Assertions.assertEquals(2, usersResult.length);
        boolean found;
        for (User userResult : usersResult) {
            found = false;
            for (User user : users) {
                if (user.getId().equals(userResult.getId()) && !user.getId().equals("not")) {
                    found = true;
                    if (!AssertUsers.usersEquals(userResult, user)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }
}