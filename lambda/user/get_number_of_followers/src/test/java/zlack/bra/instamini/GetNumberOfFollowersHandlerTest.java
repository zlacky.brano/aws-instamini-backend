package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.follow.Follow;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
class GetNumberOfFollowersHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetNumberOfFollowersHandler getNumberOfFollowersHandler;

    private final List<User> users = new ArrayList<>() {
        {
            add(new User("id", "test", "test"));
            add(new User("id2", "test2", "test2"));
            add(new User("not", "2", "3"));
        }
    };

    private final List<Follow> follows = new ArrayList<>() {
        {
            add(new Follow("id", "id", "id"));
            add(new Follow("id1", "id", "not"));
            add(new Follow("id2", "id2", "id"));
            add(new Follow("id3", "id2", "not"));
            add(new Follow("id4", "not", "id2"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getNumberOfFollowersHandler = new GetNumberOfFollowersHandler();

        Table userTable = CreateTable.create(dynamoDB, TableName.userTable);
        Table followTable = CreateTable.create(dynamoDB, TableName.followTable);

        for (User user : users) {
            userTable.putItem(CreateItem.createUserItem(user));
        }
        for (Follow follow : follows) {
            followTable.putItem(CreateItem.createFollowItem(follow));
        }
    }

    @Test
    void handleRequest() {
        Map<String, String> queryParameters = new HashMap<>() {
            {
                put("userId", "id");
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(queryParameters);
        APIGatewayProxyResponseEvent response = getNumberOfFollowersHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());
        Assertions.assertEquals(response.getBody(), "{\"result\":2}");
    }

    @Test
    void handleRequest400() {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(new HashMap<>());
        APIGatewayProxyResponseEvent response = getNumberOfFollowersHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(400, response.getStatusCode());
    }
}