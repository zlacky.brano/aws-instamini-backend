package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.follow.Follow;
import zlack.bra.instamini.data.follow.FollowCreate;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.CheckIfDocumentIdExists;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertFollows;
import zlack.bra.instamini.test.item.CreateItem;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Testcontainers
class SaveFollowHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private SaveFollowHandler saveFollowHandler;

    private final Follow followAlreadyIn = new Follow("in", "in1", "in2");

    private final FollowCreate followCreate = new FollowCreate("userWho", "userWhom");

    private final FollowCreate followCreate404UserWho = new FollowCreate("wrong", "userWhom");

    private final FollowCreate followCreate404UserWhom = new FollowCreate("userWho", "wrong");

    private final User userWho = new User("userWho", "user", "user");

    private final User userWhom = new User("userWhom", "user", "user");


    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        saveFollowHandler = new SaveFollowHandler();

        Table followTable = CreateTable.create(dynamoDB, TableName.followTable);
        Table userTable = CreateTable.create(dynamoDB, TableName.userTable);

        userTable.putItem(CreateItem.createUserItem(userWho));
        userTable.putItem(CreateItem.createUserItem(userWhom));

        followTable.putItem(CreateItem.createFollowItem(followAlreadyIn));
    }

    @Test
    void handleRequest() {
        String invokeResultUserWhoJson = "{\"statusCode\":200}";
        InvokeResult invokeResultUserWho = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserWhoJson.getBytes()));

        String invokeResultUserWhomJson = "{\"statusCode\":200}";
        InvokeResult invokeResultUserWhom = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserWhomJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate.getUserIdWho()))
                    .thenReturn(invokeResultUserWho);
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate.getUserIdWhom()))
                    .thenReturn(invokeResultUserWhom);

            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withBody(followCreate.toJson());
            APIGatewayProxyResponseEvent response = saveFollowHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(201, response.getStatusCode());

            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate.getUserIdWho()));
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate.getUserIdWhom()));
        }

        checkFollows();
    }

    @Test
    void handleRequest404UserWhom() {
        String invokeResultUserWhoJson = "{\"statusCode\":200}";
        InvokeResult invokeResultUserWho = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserWhoJson.getBytes()));

        String invokeResultUserWhomJson = "{\"statusCode\":404}";
        InvokeResult invokeResultUserWhom = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserWhomJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate404UserWhom.getUserIdWho()))
                    .thenReturn(invokeResultUserWho);
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate404UserWhom.getUserIdWhom()))
                    .thenReturn(invokeResultUserWhom);
            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withBody(followCreate404UserWhom.toJson());
            APIGatewayProxyResponseEvent response = saveFollowHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(404, response.getStatusCode());
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate404UserWhom.getUserIdWho()));
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate404UserWhom.getUserIdWhom()));
        }
    }

    @Test
    void handleRequest404UserWho() {
        String invokeResultUserWhoJson = "{\"statusCode\":404}";
        InvokeResult invokeResultUserWho = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserWhoJson.getBytes()));

        String invokeResultUserWhomJson = "{\"statusCode\":200}";
        InvokeResult invokeResultUserWhom = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserWhomJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate404UserWho.getUserIdWho()))
                    .thenReturn(invokeResultUserWho);
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate404UserWho.getUserIdWhom()))
                    .thenReturn(invokeResultUserWhom);
            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withBody(followCreate404UserWho.toJson());
            APIGatewayProxyResponseEvent response = saveFollowHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(404, response.getStatusCode());
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate404UserWho.getUserIdWho()));
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(followCreate404UserWho.getUserIdWhom()));
        }
    }

    private void checkFollows() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.followTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Follow> follows = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Follow follow = new Follow(item.get("id").getS(), item.get("userIdWho").getS(), item.get("userIdWhom").getS());
            follows.add(follow);
        }

        Assertions.assertEquals(2, follows.size());
        if (!follows.get(0).getId().equals(followAlreadyIn.getId())) {
            if (!AssertFollows.followsEquals(followCreate, follows.get(0)) ||
                    !AssertFollows.followsEquals(followAlreadyIn, follows.get(1))) {
                throw new AssertionFailedError();
            }
        } else if (follows.get(0).getId().equals(followAlreadyIn.getId())) {
            if (!AssertFollows.followsEquals(followAlreadyIn, follows.get(0)) ||
                    !AssertFollows.followsEquals(followCreate, follows.get(1))) {
                throw new AssertionFailedError();
            }
        } else {
            throw new AssertionFailedError();
        }
    }
}