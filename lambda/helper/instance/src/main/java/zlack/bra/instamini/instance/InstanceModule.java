package zlack.bra.instamini.instance;

import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.AWSLambdaClientBuilder;
import com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.AwsInfo;

public class InstanceModule {
    public static AmazonDynamoDB amazonDynamoDB = AmazonDynamoDBClientBuilder
            .standard()
            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(AwsInfo.serviceEndpoint, AwsInfo.region))
            .build();

    public static DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

    public static AWSLambda awsLambda = AWSLambdaClientBuilder
            .standard()
            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(AwsInfo.serviceEndpoint, AwsInfo.region))
            .build();

    public static ObjectMapper objectMapper = new ObjectMapper();
}
