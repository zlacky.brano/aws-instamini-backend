package zlack.bra.instamini.invoke;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import zlack.bra.instamini.instance.InstanceModule;

public class InvokeLambda {

    private final static AWSLambda client = InstanceModule.awsLambda;

    public static InvokeResult invokeWithQueryParam(String functionName, String nameOfQueryParam, String queryParam) {
        InvokeRequest request = new InvokeRequest();
        request.withFunctionName(functionName)
                .withPayload("{\"queryStringParameters\":{\"" + nameOfQueryParam + "\":\"" + queryParam + "\"}}");
        return client.invoke(request);
    }
}
