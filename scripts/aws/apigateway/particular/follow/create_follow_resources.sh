#!/bin/bash

# First argument - RestAPI ID
# Second argument - Region
# Third argument - Stage name

FOLLOWS_PATH="follows"
FOLLOWS_ID_PARAMETER="id"

echo "   - Creating resource for path /${FOLLOWS_PATH}, /${FOLLOWS_PATH}/{${FOLLOWS_ID_PARAMETER}}"
FOLLOWS_RESOURCE_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "${FOLLOWS_PATH}")
RESOURCE_ID_WITH_PARAM_ID=$(bash ./scripts/aws/apigateway/helper/create_resource.sh "$1" "$2" "{${FOLLOWS_ID_PARAMETER}}" "${FOLLOWS_RESOURCE_ID}")

echo "    - Creating method and integration for GetFollowsResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${FOLLOWS_RESOURCE_ID}" "GET" "GetFollowsFunction"

echo "    - Creating method and integration for SaveFollowResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${FOLLOWS_RESOURCE_ID}" "POST" "SaveFollowFunction"

echo "    - Creating method and integration for DeleteFollowResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "DELETE" "DeleteFollowFunction" "${FOLLOWS_ID_PARAMETER}"

echo "    - Creating method and integration for GetFollowByIdResource"
bash ./scripts/aws/apigateway/helper/create_method_and_integration.sh "$1" "$2" "${RESOURCE_ID_WITH_PARAM_ID}" "GET" "GetFollowByIdFunction" "${FOLLOWS_ID_PARAMETER}"

echo "   - Base URL: http://localhost:4566/restapis/$1/$3/_user_request_/${FOLLOWS_PATH}"