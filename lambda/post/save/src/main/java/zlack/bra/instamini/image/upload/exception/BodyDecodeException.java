package zlack.bra.instamini.image.upload.exception;

public class BodyDecodeException extends Exception {

    public BodyDecodeException(String errorMessage) {
        super(errorMessage);
    }
}
