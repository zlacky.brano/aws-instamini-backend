package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
class GetNumberOfLikesOnPostHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetNumberOfLikesOnPostHandler getNumberOfLikesOnPostHandler;

    private final List<Like> likes = new ArrayList<>() {
        {
            add(new Like("id", "test", "yes"));
            add(new Like("not", "not", "not"));
            add(new Like("1", "4", "yes"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getNumberOfLikesOnPostHandler = new GetNumberOfLikesOnPostHandler();

        Table table = CreateTable.create(dynamoDB, TableName.likeTable);

        for (Like like : likes) {
            table.putItem(CreateItem.createLikeItem(like));
        }
    }

    @Test
    void handleRequest() {
        Map<String, String> queryParameters = new HashMap<>() {
            {
                put("postId", "yes");
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(queryParameters);
        APIGatewayProxyResponseEvent response = getNumberOfLikesOnPostHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());
        Assertions.assertEquals("{\"result\":2}", response.getBody());
    }

    @Test
    void handleRequest400() {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(new HashMap<>());
        APIGatewayProxyResponseEvent response = getNumberOfLikesOnPostHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(400, response.getStatusCode());
    }
}