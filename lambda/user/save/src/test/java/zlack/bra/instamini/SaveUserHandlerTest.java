package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.data.user.UserCreate;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertUsers;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Testcontainers
class SaveUserHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private SaveUserHandler saveUserHandler;

    private final User userAlreadyIn = new User("in", "in", "in");

    private final UserCreate userCreate = new UserCreate("not", "user");


    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        saveUserHandler = new SaveUserHandler();

        Table userTable = CreateTable.create(dynamoDB, TableName.userTable);

        userTable.putItem(CreateItem.createUserItem(userAlreadyIn));
    }

    @Test
    void handleRequest() {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withBody(userCreate.toJson());
        APIGatewayProxyResponseEvent response = saveUserHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(201, response.getStatusCode());

        checkUsers();
    }

    private void checkUsers() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.userTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<User> users = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            User user = new User(item.get("id").getS(), item.get("username").getS(), item.get("email").getS());
            users.add(user);
        }

        Assertions.assertEquals(2, users.size());
        if (!users.get(0).getId().equals(userAlreadyIn.getId())) {
            if (!AssertUsers.usersEquals(userCreate, users.get(0)) ||
                    !AssertUsers.usersEquals(userAlreadyIn, users.get(1))) {
                throw new AssertionFailedError();
            }
        } else if (users.get(0).getId().equals(userAlreadyIn.getId())) {
            if (!AssertUsers.usersEquals(userCreate, users.get(1)) ||
                    !AssertUsers.usersEquals(userAlreadyIn, users.get(0))) {
                throw new AssertionFailedError();
            }
        } else {
            throw new AssertionFailedError();
        }
    }
}