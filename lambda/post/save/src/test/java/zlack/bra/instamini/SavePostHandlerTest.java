package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.data.post.PostCreate;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.image.upload.CloudinaryService;
import zlack.bra.instamini.image.upload.ImageUploadService;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.CheckIfDocumentIdExists;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertPosts;
import zlack.bra.instamini.test.item.CreateItem;
import zlack.bra.instamini.util.MultipartUtil;

import java.io.File;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.util.*;


@Testcontainers
class SavePostHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private SavePostHandler savePostHandler;

    private ImageUploadService imageUploadService = new CloudinaryService();

    private final Post postAlreadyIn = new Post("in", "in", "in", "in", "in");

    private final User user = new User("user", "user", "user");

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        savePostHandler = new SavePostHandler();

        Table postTable = CreateTable.create(dynamoDB, TableName.postTable);
        Table userTable = CreateTable.create(dynamoDB, TableName.userTable);

        userTable.putItem(CreateItem.createUserItem(user));
        postTable.putItem(CreateItem.createPostItem(postAlreadyIn));
    }

    @Test
    void handleRequest() throws IOException {
        String path = "/src/test/resources/image-test/test.png";
        String description = "test";
        String userId = user.getId();

        String invokeResultJson= "{\"statusCode\":200}";
        InvokeResult invokeResult = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultJson.getBytes()));

        MultipartUtil multipart = new MultipartUtil();
        multipart.addFormField("description", description);
        multipart.addFormField("userId", userId);
        multipart.addPngFilePart("image", new File(System.getProperty("user.dir") + path));
        multipart.finish();

        byte[] bodyBytes = multipart.getBytes();
        byte[] bodyBytesEncoded = Base64.getEncoder().encode(bodyBytes);

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(userId))
                    .thenReturn(invokeResult);

            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent().withHeaders(Map.of("Content-Type",
                            "multipart/form-data; boundary=" + multipart.getBoundary()))
                    .withBody(new String(bodyBytesEncoded));
            APIGatewayProxyResponseEvent response = savePostHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(201, response.getStatusCode());

            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(userId));
        }

        checkPosts(new PostCreate(description, userId, null));
    }

    @Test
    void handleRequest404User() throws IOException {
        String path = "/src/test/resources/image-test/test.png";
        String description = "test";
        String userId = "does not exist";

        MultipartUtil multipart = new MultipartUtil();
        multipart.addFormField("description", description);
        multipart.addFormField("userId", userId);
        multipart.addPngFilePart("image", new File(System.getProperty("user.dir") + path));
        multipart.finish();

        byte[] bodyBytes = multipart.getBytes();
        byte[] bodyBytesEncoded = Base64.getEncoder().encode(bodyBytes);

        String invokeResultJson = "{\"statusCode\":404}";
        InvokeResult invokeResultUser = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(userId))
                    .thenReturn(invokeResultUser);
            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent().withHeaders(Map.of("Content-Type",
                            "multipart/form-data; boundary=" + multipart.getBoundary()))
                    .withBody(new String(bodyBytesEncoded));
            APIGatewayProxyResponseEvent response = savePostHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(404, response.getStatusCode());
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(userId));
        }
    }

    @Test
    void handleRequest400NoMultipart() {
        String userId = "test";

        String invokeResultJson = "{\"statusCode\":404}";
        InvokeResult invokeResultUser = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(userId))
                    .thenReturn(invokeResultUser);
            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent().withHeaders(Map.of("Content-Type",
                            "application/json"))
                    .withBody("");
            APIGatewayProxyResponseEvent response = savePostHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(400, response.getStatusCode());
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(userId), Mockito.times(0));
        }
    }

    @Test
    void handleRequest400NoContentType() {
        String userId = "test";

        String invokeResultUserJson = "{\"statusCode\":404}";
        InvokeResult invokeResultUser = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(userId))
                    .thenReturn(invokeResultUser);
            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent().withHeaders(Map.of())
                    .withBody("");
            APIGatewayProxyResponseEvent response = savePostHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(400, response.getStatusCode());
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(userId), Mockito.times(0));
        }
    }

    @Test
    void handleRequest400MissingBodyKey() throws IOException {
        String path = "/src/test/resources/image-test/test.png";
        String userId = user.getId();

        String invokeResultJson = "{\"statusCode\":404}";
        InvokeResult invokeResult = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultJson.getBytes()));

        MultipartUtil multipart = new MultipartUtil();
        multipart.addFormField("userId", userId);
        multipart.addPngFilePart("image", new File(System.getProperty("user.dir") + path));
        multipart.finish();

        byte[] bodyBytes = multipart.getBytes();
        byte[] bodyBytesEncoded = Base64.getEncoder().encode(bodyBytes);

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(userId))
                    .thenReturn(invokeResult);

            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent().withHeaders(Map.of("Content-Type",
                            "multipart/form-data; boundary=" + multipart.getBoundary()))
                    .withBody(new String(bodyBytesEncoded));
            APIGatewayProxyResponseEvent response = savePostHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(400, response.getStatusCode());

            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(userId), Mockito.times(0));
        }
    }

    @Test
    void handleRequest400NotImage() throws IOException {
        String path = "/src/test/resources/image-test/not_image.txt";
        String description = "test";
        String userId = user.getId();

        String invokeResultJson = "{\"statusCode\":200}";
        InvokeResult invokeResult = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultJson.getBytes()));

        MultipartUtil multipart = new MultipartUtil();
        multipart.addFormField("description", description);
        multipart.addFormField("userId", userId);
        multipart.addPngFilePart("image", new File(System.getProperty("user.dir") + path));
        multipart.finish();

        byte[] bodyBytes = multipart.getBytes();
        byte[] bodyBytesEncoded = Base64.getEncoder().encode(bodyBytes);

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(userId))
                    .thenReturn(invokeResult);

            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent().withHeaders(Map.of("Content-Type",
                            "multipart/form-data; boundary=" + multipart.getBoundary()))
                    .withBody(new String(bodyBytesEncoded));
            APIGatewayProxyResponseEvent response = savePostHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(400, response.getStatusCode());

            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(userId));
        }
    }

    private void checkPosts(PostCreate postCreate) throws IOException {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.postTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Post> posts = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Post post = new Post(item.get("id").getS(), item.get("description").getS(), item.get("photoUrl").getS(), item.get("time").getS(), item.get("userId").getS());
            posts.add(post);
            if (!Objects.equals(post.getId(), postAlreadyIn.getId())) {
                String photoUrl = post.getPhotoUrl();
                String publicId = photoUrl.substring(photoUrl.lastIndexOf("/") + 1, photoUrl.lastIndexOf("."));
                imageUploadService.destroy(publicId);
            }
        }

        Assertions.assertEquals(2, posts.size());
        if (!posts.get(0).getId().equals(postAlreadyIn.getId())) {
            if (!AssertPosts.postsEquals(postCreate, posts.get(0)) ||
                    !AssertPosts.postsEquals(postAlreadyIn, posts.get(1))) {
                throw new AssertionFailedError();
            }
        } else if (posts.get(0).getId().equals(postAlreadyIn.getId())) {
            if (!AssertPosts.postsEquals(postCreate, posts.get(1)) ||
                    !AssertPosts.postsEquals(postAlreadyIn, posts.get(0))) {
                throw new AssertionFailedError();
            }
        } else {
            throw new AssertionFailedError();
        }
    }
}