package zlack.bra.instamini.image.upload;

import com.cloudinary.Cloudinary;
import zlack.bra.instamini.image.upload.exception.UploadException;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.Collections;
import java.util.Map;
import java.util.UUID;


public class CloudinaryService implements ImageUploadService {

    private final String cloudName = "instamini";

    // test instance secret and key for demonstration purposes
    private final String apiKey = "875182531596125";
    private final String apiSecret = "f7Dn3RJcypBXpOcRcMUBGqtJs8E";

    private final Cloudinary cloudinary = new Cloudinary(Cloudinary.asMap(
            "cloud_name", cloudName,
            "api_key", apiKey,
            "api_secret", apiSecret));


    @Override
    public String upload(byte[] file, Map options) throws IOException, UploadException {
        ByteArrayInputStream stream = new ByteArrayInputStream(file);

        BufferedImage image = ImageIO.read(stream);
        if (image == null) {
            throw new UploadException("Error: File is not an image!");
        }

        Map result = cloudinary.uploader().upload(file, options);

        return result.get("url").toString();
    }

    @Override
    public String upload(byte[] file) throws IOException, UploadException {
        return upload(file, Collections.emptyMap());
    }

    @Override
    public void destroy(String publicId, Map options) throws IOException {
        cloudinary.uploader().destroy(publicId, options);
    }

    @Override
    public void destroy(String publicId) throws IOException {
        destroy(publicId, Collections.emptyMap());
    }
}
