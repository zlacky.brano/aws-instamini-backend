package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.InvokeLambda;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertComments;
import zlack.bra.instamini.test.assertions.AssertLikes;
import zlack.bra.instamini.test.assertions.AssertPosts;
import zlack.bra.instamini.test.item.CreateItem;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
public class DeletePostHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private DeletePostHandler deletePostHandler;

    private ObjectMapper objectMapper = InstanceModule.objectMapper;

    private final Post postToDelete = new Post("id", "test", "test", "test", "test");

    private final Post post = new Post("not", "not", "not", "not", "not");

    private final List<Comment> commentsToDelete = new ArrayList<>() {
        {
            add(new Comment("idComment", "test", "test", "test", "id"));
            add(new Comment("a", "a", "a", "a", "id"));
        }
    };

    private final List<Comment> comments = new ArrayList<>() {
        {
            add(new Comment("idComment2", "test", "test", "test", "not"));
            add(new Comment("a2", "a", "a", "a", "not"));
        }
    };

    private final List<Like> likesToDelete = new ArrayList<>() {
        {
            add(new Like("idLike", "test", "id"));
            add(new Like("a", "test", "id"));
        }
    };

    private final List<Like> likes = new ArrayList<>() {
        {
            add(new Like("idLike2", "test", "not"));
            add(new Like("a2", "a", "not"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        deletePostHandler = new DeletePostHandler();

        Table postTable = CreateTable.create(dynamoDB, TableName.postTable);
        Table commentTable = CreateTable.create(dynamoDB, TableName.commentTable);
        Table likeTable = CreateTable.create(dynamoDB, TableName.likeTable);

        postTable.putItem(CreateItem.createPostItem(postToDelete));
        postTable.putItem(CreateItem.createPostItem(post));

        for (Comment comment: commentsToDelete) {
            commentTable.putItem(CreateItem.createCommentItem(comment));
        }
        for (Comment comment: comments) {
            commentTable.putItem(CreateItem.createCommentItem(comment));
        }
        for (Like like: likesToDelete) {
            likeTable.putItem(CreateItem.createLikeItem(like));
        }
        for (Like like: likes) {
            likeTable.putItem(CreateItem.createLikeItem(like));
        }
    }

    @Test
    public void handleRequest() throws JsonProcessingException {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", postToDelete.getId());
            }
        };

        String commentsJson = objectMapper.writeValueAsString(commentsToDelete);
        commentsJson = "{\"body\":[" + commentsJson + "], \"statusCode\":200}";
        InvokeResult commentsOnPostResult = new InvokeResult().withPayload(ByteBuffer.wrap(commentsJson.getBytes()));

        String likesJson = objectMapper.writeValueAsString(likesToDelete);
        likesJson = "{\"body\":[" + likesJson + "], \"statusCode\":200}";
        InvokeResult likesOnPostResult = new InvokeResult().withPayload(ByteBuffer.wrap(likesJson.getBytes()));

        try (MockedStatic<InvokeLambda> utilities = Mockito.mockStatic(InvokeLambda.class)) {
            utilities.when(() -> InvokeLambda.invokeWithQueryParam("CommentsOnPostFunction", "postId", postToDelete.getId()))
                    .thenReturn(commentsOnPostResult);
            utilities.when(() -> InvokeLambda.invokeWithQueryParam("GetLikesOnPostFunction", "postId", postToDelete.getId()))
                    .thenReturn(likesOnPostResult);

            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withPathParameters(pathParameters);
            APIGatewayProxyResponseEvent response = deletePostHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(200, response.getStatusCode());

            utilities.verify(() -> InvokeLambda.invokeWithQueryParam("CommentsOnPostFunction", "postId", postToDelete.getId()));
            utilities.verify(() ->  InvokeLambda.invokeWithQueryParam("GetLikesOnPostFunction", "postId", postToDelete.getId()));
        }

        checkDeletedPosts();
        checkDeletedLikes();
        checkDeletedComments();
    }

    private void checkDeletedPosts() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.postTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Post> posts = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Post post = new Post(item.get("id").getS(), item.get("description").getS(), item.get("photoUrl").getS(), item.get("time").getS(), item.get("userId").getS());
            posts.add(post);
        }

        Assertions.assertEquals(1, posts.size());
        if (!AssertPosts.postsEquals(post, posts.get(0))) {
            throw new AssertionFailedError();
        }
    }

    private void checkDeletedComments() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.commentTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Comment> commentsScan = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Comment comment = new Comment(item.get("id").getS(), item.get("text").getS(), item.get("time").getS(), item.get("userId").getS(), item.get("postId").getS());
            commentsScan.add(comment);
        }

        Assertions.assertEquals(2, commentsScan.size());
        boolean found;
        for (Comment commentScan : commentsScan) {
            found = false;
            for (Comment comment : comments) {
                if (comment.getId().equals(commentScan.getId())) {
                    found = true;
                    if (!AssertComments.commentsEquals(commentScan, comment)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }

    private void checkDeletedLikes() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.likeTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Like> likesScan = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Like like = new Like(item.get("id").getS(), item.get("userId").getS(), item.get("postId").getS());
            likesScan.add(like);
        }

        Assertions.assertEquals(2, likesScan.size());
        boolean found;
        for (Like likeScan : likesScan) {
            found = false;
            for (Like like : likes) {
                if (like.getId().equals(likeScan.getId())) {
                    found = true;
                    if (!AssertLikes.likesEquals(likeScan, like)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }
}