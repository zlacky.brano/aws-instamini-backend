package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.data.like.LikeCreate;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.CheckIfDocumentIdExists;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertLikes;
import zlack.bra.instamini.test.item.CreateItem;

import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;


@Testcontainers
class SaveLikeHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private SaveLikeHandler saveLikeHandler;

    private final Like likeAlreadyIn = new Like("in", "in", "in");

    private final LikeCreate likeCreate = new LikeCreate("not", "user");

    private final LikeCreate likeCreate404User = new LikeCreate("wrong", "post");

    private final LikeCreate likeCreate404Post = new LikeCreate("user", "wrong");

    private final User user = new User("user", "user", "user");

    private final Post post = new Post("post", "post", "post", "post", "post");



    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        saveLikeHandler = new SaveLikeHandler();

        Table likeTable = CreateTable.create(dynamoDB, TableName.likeTable);
        Table userTable = CreateTable.create(dynamoDB, TableName.userTable);
        Table postTable = CreateTable.create(dynamoDB, TableName.postTable);

        userTable.putItem(CreateItem.createUserItem(user));
        postTable.putItem(CreateItem.createPostItem(post));
        likeTable.putItem(CreateItem.createLikeItem(likeAlreadyIn));
    }

    @Test
    void handleRequest() {
        String invokeResultUserJson = "{\"statusCode\":200}";
        InvokeResult invokeResultUser = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserJson.getBytes()));

        String invokeResultPostJson = "{\"statusCode\":200}";
        InvokeResult invokeResultPost = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultPostJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfPostExists(likeCreate.getPostId()))
                    .thenReturn(invokeResultPost);
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(likeCreate.getUserId()))
                    .thenReturn(invokeResultUser);

            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withBody(likeCreate.toJson());
            APIGatewayProxyResponseEvent response = saveLikeHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(201, response.getStatusCode());

            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(likeCreate.getUserId()));
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfPostExists(likeCreate.getPostId()));
        }

        checkLikes();
    }

    @Test
    void handleRequest404User() {
        String invokeResultUserJson = "{\"statusCode\":404}";
        InvokeResult invokeResultUser = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserJson.getBytes()));

        String invokeResultPostJson = "{\"statusCode\":200}";
        InvokeResult invokeResultPost = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultPostJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfPostExists(likeCreate404User.getPostId()))
                    .thenReturn(invokeResultPost);
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(likeCreate404User.getUserId()))
                    .thenReturn(invokeResultUser);
            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withBody(likeCreate404User.toJson());
            APIGatewayProxyResponseEvent response = saveLikeHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(404, response.getStatusCode());
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(likeCreate404User.getUserId()));
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfPostExists(likeCreate404User.getPostId()));
        }
    }

    @Test
    void handleRequest404Post() {
        String invokeResultUserJson = "{\"statusCode\":200}";
        InvokeResult invokeResultUser = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultUserJson.getBytes()));

        String invokeResultPostJson = "{\"statusCode\":404}";
        InvokeResult invokeResultPost = new InvokeResult().withPayload(ByteBuffer.wrap(invokeResultPostJson.getBytes()));

        try (MockedStatic<CheckIfDocumentIdExists> utilities = Mockito.mockStatic(CheckIfDocumentIdExists.class)) {
            utilities.when(() -> CheckIfDocumentIdExists.checkIfPostExists(likeCreate404Post.getPostId()))
                    .thenReturn(invokeResultPost);
            utilities.when(() -> CheckIfDocumentIdExists.checkIfUserExists(likeCreate404Post.getUserId()))
                    .thenReturn(invokeResultUser);
            APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                    .withBody(likeCreate404Post.toJson());
            APIGatewayProxyResponseEvent response = saveLikeHandler.handleRequest(request, new TestContext());

            Assertions.assertEquals(404, response.getStatusCode());
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfUserExists(likeCreate404Post.getUserId()));
            utilities.verify(() -> CheckIfDocumentIdExists.checkIfPostExists(likeCreate404Post.getPostId()));
        }
    }

    private void checkLikes() {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.likeTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Like> likes = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Like like = new Like(item.get("id").getS(), item.get("userId").getS(), item.get("postId").getS());
            likes.add(like);
        }

        Assertions.assertEquals(2, likes.size());
        if (!likes.get(0).getId().equals(likeAlreadyIn.getId())) {
            if (!AssertLikes.likesEquals(likeCreate, likes.get(0)) ||
                    !AssertLikes.likesEquals(likeAlreadyIn, likes.get(1))) {
                throw new AssertionFailedError();
            }
        } else if (likes.get(0).getId().equals(likeAlreadyIn.getId())) {
            if (!AssertLikes.likesEquals(likeCreate, likes.get(1)) ||
                    !AssertLikes.likesEquals(likeAlreadyIn, likes.get(0))) {
                throw new AssertionFailedError();
            }
        } else {
            throw new AssertionFailedError();
        }
    }
}