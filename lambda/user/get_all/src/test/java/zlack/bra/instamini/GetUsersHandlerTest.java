package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertUsers;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.List;


@Testcontainers
class GetUsersHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetUsersHandler getUsersHandler;

    private final List<User> users = new ArrayList<>() {
        {
            add(new User("id", "test", "test"));
            add(new User("not", "not", "not"));
            add(new User("1", "2", "3"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getUsersHandler = new GetUsersHandler();

        Table table = CreateTable.create(dynamoDB, TableName.userTable);

        for (User user : users) {
            table.putItem(CreateItem.createUserItem(user));
        }
    }

    @Test
    void handleRequest() throws JsonProcessingException {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent();
        APIGatewayProxyResponseEvent response = getUsersHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkUsers(response.getBody());
    }

    private void checkUsers(String bodyResult) throws JsonProcessingException {
        User[] usersResult = new ObjectMapper().readValue(bodyResult, User[].class);

        Assertions.assertEquals(3, usersResult.length);
        boolean found;
        for (User userResult : usersResult) {
            found = false;
            for (User user : users) {
                if (user.getId().equals(userResult.getId())) {
                    found = true;
                    if (!AssertUsers.usersEquals(userResult, user)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }
}
