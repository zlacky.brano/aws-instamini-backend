package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.follow.FollowCreate;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.CheckIfDocumentIdExists;

import java.util.*;


public class SaveFollowHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;
    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    /**
     * Saves follow
     * @param event all information about request
     * @param context request context
     * @return 201, if everything was successful or 404, if any of users was not found
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            FollowCreate follow = objectMapper.readValue(event.getBody(), FollowCreate.class);

            InvokeResult userWhoGetByIdResult = CheckIfDocumentIdExists.checkIfUserExists(follow.getUserIdWho());
            InvokeResult userWhomGetByIdResult = CheckIfDocumentIdExists.checkIfUserExists(follow.getUserIdWhom());
            if (userWhoGetByIdResult.getFunctionError() != null || userWhomGetByIdResult.getFunctionError() != null) {
                return response.withStatusCode(500).withBody("{}");
            }

            int statusCodeUserWho = objectMapper.readTree(new ByteBufferBackedInputStream(userWhoGetByIdResult.getPayload())).get("statusCode").asInt();
            int statusCodeUserWhom = objectMapper.readTree(new ByteBufferBackedInputStream(userWhomGetByIdResult.getPayload())).get("statusCode").asInt();
            if (statusCodeUserWho != 200) {
                return response.withStatusCode(statusCodeUserWho).withBody("{\"errorMessage\":\"User with id userWhoId does not exist or there was a server error.\"}");
            } else if (statusCodeUserWhom != 200) {
                return response.withStatusCode(statusCodeUserWhom).withBody("{\"errorMessage\":\"User with id userWhomId does not exist or there was a server error.\"}");
            }

            Map<String, AttributeValue> followMap = new HashMap<>();
            followMap.put("id", new AttributeValue(UUID.randomUUID().toString()));
            followMap.put("userIdWho", new AttributeValue(follow.getUserIdWho()));
            followMap.put("userIdWhom", new AttributeValue(follow.getUserIdWhom()));

            PutItemRequest putItemRequest = new PutItemRequest()
                    .withTableName(TableName.followTable).withItem(followMap);

            client.putItem(putItemRequest);
            final String result = "{\"message\": \"Follow created with id " + followMap.get("id").getS() + "\"}";
            return response.withStatusCode(201).withBody(result);
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
