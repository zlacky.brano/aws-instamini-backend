package zlack.bra.instamini.test.assertions;

import zlack.bra.instamini.data.follow.Follow;
import zlack.bra.instamini.data.follow.FollowCreate;

public class AssertFollows {

    public static boolean followsEquals(Follow follow1, Follow follow2) {
        return follow1.getId().equals(follow2.getId()) &&
                follow1.getUserIdWho().equals(follow2.getUserIdWho()) &&
                follow1.getUserIdWhom().equals(follow2.getUserIdWhom());
    }

    public static boolean followsEquals(FollowCreate followCreate, Follow follow) {
        return !follow.getId().isBlank() &&
                follow.getUserIdWhom().equals(followCreate.getUserIdWhom()) &&
                follow.getUserIdWho().equals(followCreate.getUserIdWho());
    }
}
