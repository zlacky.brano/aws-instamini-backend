package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertPosts;
import zlack.bra.instamini.test.item.CreateItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Testcontainers
class GetPostByIdHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private GetPostByIdHandler getPostByIdHandler;

    private final List<Post> posts = new ArrayList<>() {
        {
            add(new Post("id", "test", "test", "test", "test"));
            add(new Post("not", "not", "not", "not", "not"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        getPostByIdHandler = new GetPostByIdHandler();

        Table table = CreateTable.create(dynamoDB, TableName.postTable);

        for (Post post : posts) {
            table.putItem(CreateItem.createPostItem(post));
        }
    }

    @Test
    void handleRequest() throws IOException {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", posts.get(0).getId());
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters);
        APIGatewayProxyResponseEvent response = getPostByIdHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());
        Post postResult = new ObjectMapper().readValue(response.getBody(), Post.class);
        if (!AssertPosts.postsEquals(posts.get(0), postResult)) {
            throw new AssertionFailedError();
        }
    }

    @Test
    void handleRequest404() {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", "does not exist");
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters);
        APIGatewayProxyResponseEvent response = getPostByIdHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(404, response.getStatusCode());
    }
}