package zlack.bra.instamini.image.upload.exception;

public class UploadException extends Exception {

    public UploadException(String errorMessage) {
        super(errorMessage);
    }
}
