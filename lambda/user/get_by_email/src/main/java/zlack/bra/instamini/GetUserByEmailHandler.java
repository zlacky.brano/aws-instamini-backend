package zlack.bra.instamini;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.instance.InstanceModule;


public class GetUserByEmailHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final DynamoDB dynamoDB = InstanceModule.dynamoDB;

    /**
     * Gets user by email
     * @param event all information about request
     * @param context request context
     * @return 200, if successful, 404, if user was not found, 400, if query param email is missing
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            Table table = dynamoDB.getTable(TableName.userTable);

            Map<String, String> queryParameters = event.getQueryStringParameters();
            if (queryParameters.isEmpty() || !queryParameters.containsKey("email")) {
                return response.withStatusCode(400).withBody("{\"errorMessage\":\"Missing query parameter email\"}");
            }

            ItemCollection<ScanOutcome> result = table.scan(new ScanFilter("email").eq(queryParameters.get("email")));

            if (result.firstPage().size() == 0) {
                return response.withStatusCode(404);
            }
            return response.withStatusCode(200).withBody(result.firstPage().iterator().next().toJSONPretty());
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
