package zlack.bra.instamini.test.assertions;

import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.data.comment.CommentCreate;

public class AssertComments {

    public static boolean commentsEquals(Comment comment1, Comment comment2) {
        return comment1.getId().equals(comment2.getId()) &&
                comment1.getPostId().equals(comment2.getPostId()) &&
                comment1.getUserId().equals(comment2.getUserId()) &&
                comment1.getText().equals(comment2.getText()) &&
                comment1.getTime().equals(comment2.getTime());
    }

    public static boolean commentsEquals(Comment comment1, Comment comment2, String newText) {
        return comment1.getId().equals(comment2.getId()) &&
                comment1.getPostId().equals(comment2.getPostId()) &&
                comment1.getUserId().equals(comment2.getUserId()) &&
                newText.equals(comment2.getText()) &&
                comment1.getTime().equals(comment2.getTime());
    }

    public static boolean commentsEquals(CommentCreate commentCreate, Comment comment) {
        return !comment.getId().isBlank() &&
                commentCreate.getPostId().equals(comment.getPostId()) &&
                commentCreate.getUserId().equals(comment.getUserId()) &&
                commentCreate.getText().equals(comment.getText()) &&
                !comment.getTime().isBlank();
    }
}
