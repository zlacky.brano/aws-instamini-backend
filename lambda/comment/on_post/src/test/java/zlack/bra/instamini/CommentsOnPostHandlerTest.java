package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.opentest4j.AssertionFailedError;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertComments;
import zlack.bra.instamini.test.item.CreateItem;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Testcontainers
class CommentsOnPostHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private CommentsOnPostHandler commentsOnPostHandler;

    private final List<Comment> comments = new ArrayList<>() {
        {
            add(new Comment("id", "test", "test", "test", "yes"));
            add(new Comment("not", "not", "not", "not", "not"));
            add(new Comment("1", "2", "3", "4", "yes"));
        }
    };

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        commentsOnPostHandler = new CommentsOnPostHandler();

        Table table = CreateTable.create(dynamoDB, TableName.commentTable);

        for (Comment comment : comments) {
            table.putItem(CreateItem.createCommentItem(comment));
        }
    }

    @Test
    void handleRequest() throws JsonProcessingException {
        Map<String, String> queryParameters = new HashMap<>() {
            {
                put("postId", "yes");
            }
        };
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(queryParameters);
        APIGatewayProxyResponseEvent response = commentsOnPostHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());

        checkComments(response.getBody());
    }

    @Test
    void handleRequest400() {
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withQueryStringParameters(new HashMap<>());
        APIGatewayProxyResponseEvent response = commentsOnPostHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(400, response.getStatusCode());
    }

    private void checkComments(String bodyResult) throws JsonProcessingException {
        Comment[] commentsResult = new ObjectMapper().readValue(bodyResult, Comment[].class);

        Assertions.assertEquals(2, commentsResult.length);
        boolean found;
        for (Comment commentResult : commentsResult) {
            found = false;
            for (Comment comment : comments) {
                if (comment.getId().equals(commentResult.getId()) && !comment.getPostId().equals("not")) {
                    found = true;
                    if (!AssertComments.commentsEquals(commentResult, comment)) {
                        throw new AssertionFailedError();
                    }
                }
            }
            if (!found) {
                throw new AssertionFailedError();
            }
        }
    }
}