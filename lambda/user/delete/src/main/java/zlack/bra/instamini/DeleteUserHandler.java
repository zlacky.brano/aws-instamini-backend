package zlack.bra.instamini;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.DeleteItemRequest;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.delete.DeleteUserChildren;
import zlack.bra.instamini.instance.InstanceModule;


public class DeleteUserHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;

    /**
     * Deletes user and all his children
     * @param event all information about request
     * @param context request context
     * @return 200, if it was successful
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent();

        try {
            Map<String, String> pathParameters = event.getPathParameters();

            DeleteUserChildren.delete(pathParameters.get("id"));

            Map<String, AttributeValue> idToDelete = new HashMap<>();
            idToDelete.put("id", new AttributeValue(pathParameters.get("id")));

            DeleteItemRequest deleteItemRequest = new DeleteItemRequest()
                    .withTableName(TableName.userTable).withKey(idToDelete);

            client.deleteItem(deleteItemRequest);
            return response.withStatusCode(200);
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
