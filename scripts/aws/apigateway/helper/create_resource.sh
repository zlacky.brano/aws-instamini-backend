#!/bin/bash

# First argument - RestAPI ID
# Second argument - Region
# Third argument - path
# Fourth argument (Optional) - Parent id, needed when you want to create child resource of another (for example you have resource with path /users and you want to create sub-resource /users/{id}

# Returns created resource's id

if [ -z "$4" ]
  then
    PARENT_ID=$(docker-compose run --rm aws-cli apigateway get-resources --region "$2" --rest-api-id "$1" --endpoint-url=http://localstack:4566 | jq '.items[] | select(.path == "/") | .id' | tr -d '"')
  else
    PARENT_ID="$4"
fi

docker-compose run --rm aws-cli apigateway create-resource --region "$2" --rest-api-id "$1" --parent-id "${PARENT_ID}" --path-part "$3" --endpoint-url=http://localstack:4566 | jq '.id' | tr -d '"'
