package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertComments;
import zlack.bra.instamini.test.item.CreateItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
class EditTextHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private EditTextHandler editTextHandler;

    private final Comment commentToEdit = new Comment("id", "test", "test", "test", "test");

    private final Comment comment = new Comment("not", "not", "not", "not", "not");

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        editTextHandler = new EditTextHandler();

        Item commentToDeleteItem = CreateItem.createCommentItem(commentToEdit);
        Item commentItem = CreateItem.createCommentItem(comment);

        Table table = CreateTable.create(dynamoDB, TableName.commentTable);
        table.putItem(commentToDeleteItem);
        table.putItem(commentItem);
    }

    @Test
    void handleRequest() throws IOException {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", commentToEdit.getId());
            }
        };

        String newText = "new text";
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters).withBody(getBody(newText));
        APIGatewayProxyResponseEvent response = editTextHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());
        Comment commentResult = new ObjectMapper().readValue(response.getBody(), Comment.class);
        if (!AssertComments.commentsEquals(commentToEdit, commentResult, newText)) {
            throw new AssertionFailedError();
        }

        checkEdited(newText);
    }

    @Test
    void handleRequest404() {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", "does not exist");
            }
        };
        String newText = "new text";
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters).withBody(getBody(newText));
        APIGatewayProxyResponseEvent response = editTextHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(404, response.getStatusCode());
    }

    private String getBody(String newText) {
        return "{\"text\":\"" + newText + "\"}";
    }

    private void checkEdited(String newText) {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.commentTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Comment> comments = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Comment comment = new Comment(item.get("id").getS(), item.get("text").getS(), item.get("time").getS(), item.get("userId").getS(), item.get("postId").getS());
            comments.add(comment);
        }

        Assertions.assertEquals(2, comments.size());
        if (comments.get(0).getId().equals(commentToEdit.getId())) {
            if (!AssertComments.commentsEquals(commentToEdit, comments.get(0), newText) ||
                    !AssertComments.commentsEquals(comment, comments.get(1))) {
                throw new AssertionFailedError();
            }
        } else if (comments.get(0).getId().equals(comment.getId())) {
            if (!AssertComments.commentsEquals(commentToEdit, comments.get(1), newText) ||
                    !AssertComments.commentsEquals(comment, comments.get(0))) {
                throw new AssertionFailedError();
            }
        } else {
            throw new AssertionFailedError();
        }
    }
}