package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertUsers;
import zlack.bra.instamini.test.item.CreateItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@Testcontainers
class EditUsernameHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private EditUsernameHandler editUsernameHandler;

    private final User userToEdit = new User("id", "test", "test");

    private final User user = new User("not", "not", "not");

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        editUsernameHandler = new EditUsernameHandler();

        Item userToDeleteItem = CreateItem.createUserItem(userToEdit);
        Item userItem = CreateItem.createUserItem(user);

        Table table = CreateTable.create(dynamoDB, TableName.userTable);
        table.putItem(userToDeleteItem);
        table.putItem(userItem);
    }

    @Test
    void handleRequest() throws IOException {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", userToEdit.getId());
            }
        };

        String newUsername = "new username";
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters).withBody(getBody(newUsername));
        APIGatewayProxyResponseEvent response = editUsernameHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());
        User userResult = new ObjectMapper().readValue(response.getBody(), User.class);
        if (!AssertUsers.usersEquals(userToEdit, userResult, newUsername)) {
            throw new AssertionFailedError();
        }

        checkEdited(newUsername);
    }

    @Test
    void handleRequest404() {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", "does not exist");
            }
        };
        String newUsername = "new username";
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters).withBody(getBody(newUsername));
        APIGatewayProxyResponseEvent response = editUsernameHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(404, response.getStatusCode());
    }

    private String getBody(String newUsername) {
        return "{\"username\":\"" + newUsername + "\"}";
    }

    private void checkEdited(String newText) {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.userTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<User> users = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            User user = new User(item.get("id").getS(), item.get("username").getS(), item.get("email").getS());
            users.add(user);
        }

        Assertions.assertEquals(2, users.size());
        if (users.get(0).getId().equals(userToEdit.getId())) {
            if (!AssertUsers.usersEquals(userToEdit, users.get(0), newText) ||
                    !AssertUsers.usersEquals(user, users.get(1))) {
                throw new AssertionFailedError();
            }
        } else if (users.get(0).getId().equals(user.getId())) {
            if (!AssertUsers.usersEquals(userToEdit, users.get(1), newText) ||
                    !AssertUsers.usersEquals(user, users.get(0))) {
                throw new AssertionFailedError();
            }
        } else {
            throw new AssertionFailedError();
        }
    }

}