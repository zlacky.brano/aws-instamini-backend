package zlack.bra.instamini;

import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.UpdateItemOutcome;
import com.amazonaws.services.dynamodbv2.document.spec.UpdateItemSpec;
import com.amazonaws.services.dynamodbv2.document.utils.NameMap;
import com.amazonaws.services.dynamodbv2.document.utils.ValueMap;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.post.PostEditDescription;
import zlack.bra.instamini.instance.InstanceModule;


public class EditDescriptionHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final DynamoDB dynamoDB = InstanceModule.dynamoDB;

    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    /**
     * Edits description of post
     * @param event all information about request
     * @param context request context
     * @return 200, if successful or 404, if post was not found
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);

        try {
            PostEditDescription post = objectMapper.readValue(event.getBody(), PostEditDescription.class);

            Map<String, String> pathParameters = event.getPathParameters();

            Table table = dynamoDB.getTable(TableName.postTable);

            UpdateItemSpec updateItemSpec = new UpdateItemSpec().withPrimaryKey("id", pathParameters.get("id"))
                    .withUpdateExpression("set description = :d")
                    .withValueMap(new ValueMap().withString(":d", post.getDescription()))
                    .withReturnValues(ReturnValue.ALL_NEW)
                    .withConditionExpression("attribute_exists(#id)")
                    .withNameMap(new NameMap().with("#id", "id"));

            UpdateItemOutcome result = table.updateItem(updateItemSpec);
            return response.withStatusCode(200).withBody(result.getItem().toJSONPretty());
        } catch (final ConditionalCheckFailedException e) {
            return response.withStatusCode(404).withBody("{\"errorMessage\":\"Post with given id does not exist\"}");
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
