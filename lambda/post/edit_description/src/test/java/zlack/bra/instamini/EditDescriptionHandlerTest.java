package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.ScanRequest;
import com.amazonaws.services.dynamodbv2.model.ScanResult;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import junit.framework.AssertionFailedError;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.testcontainers.dynamodb.DynaliteContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.shaded.com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.test.CreateTable;
import zlack.bra.instamini.test.TestContext;
import zlack.bra.instamini.test.assertions.AssertPosts;
import zlack.bra.instamini.test.item.CreateItem;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.*;

@Testcontainers
class EditDescriptionHandlerTest {

    @Container
    public DynaliteContainer dynaliteContainer = new DynaliteContainer();

    private AmazonDynamoDB amazonDynamoDB;

    private EditDescriptionHandler editDescriptionHandler;

    private final Post postToEdit = new Post("id", "test", "test", "test", "test");

    private final Post post = new Post("not", "not", "not", "not", "not");

    @BeforeEach
    public void setUp() throws InterruptedException {
        amazonDynamoDB = dynaliteContainer.getClient();
        DynamoDB dynamoDB = new DynamoDB(amazonDynamoDB);

        InstanceModule.amazonDynamoDB = amazonDynamoDB;
        InstanceModule.dynamoDB = dynamoDB;

        editDescriptionHandler = new EditDescriptionHandler();

        Item postToDeleteItem = CreateItem.createPostItem(postToEdit);
        Item postItem = CreateItem.createPostItem(post);

        Table table = CreateTable.create(dynamoDB, TableName.postTable);
        table.putItem(postToDeleteItem);
        table.putItem(postItem);
    }

    @Test
    void handleRequest() throws IOException {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", postToEdit.getId());
            }
        };

        String newDescription = "new description";
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters).withBody(getBody(newDescription));
        APIGatewayProxyResponseEvent response = editDescriptionHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(200, response.getStatusCode());
        Post postResult = new ObjectMapper().readValue(response.getBody(), Post.class);
        if (!AssertPosts.postsEquals(postToEdit, postResult, newDescription)) {
            throw new AssertionFailedError();
        }

        checkEdited(newDescription);
    }

    @Test
    void handleRequest404() {
        Map<String, String> pathParameters = new HashMap<>() {
            {
                put("id", "does not exist");
            }
        };
        String newDescription = "new description";
        APIGatewayProxyRequestEvent request = new APIGatewayProxyRequestEvent()
                .withPathParameters(pathParameters).withBody(getBody(newDescription));
        APIGatewayProxyResponseEvent response = editDescriptionHandler.handleRequest(request, new TestContext());

        Assertions.assertEquals(404, response.getStatusCode());
    }

    private String getBody(String newText) {
        return "{\"description\":\"" + newText + "\"}";
    }

    private void checkEdited(String newText) {
        ScanRequest scanRequest = new ScanRequest()
                .withTableName(TableName.postTable);

        ScanResult scanResult = amazonDynamoDB.scan(scanRequest);
        List<Post> posts = new ArrayList<>();
        for (Map<String, AttributeValue> item : scanResult.getItems()) {
            Post post = new Post(item.get("id").getS(), item.get("description").getS(), item.get("photoUrl").getS(), item.get("time").getS(), item.get("userId").getS());
            posts.add(post);
        }

        Assertions.assertEquals(2, posts.size());
        if (posts.get(0).getId().equals(postToEdit.getId())) {
            if (!AssertPosts.postsEquals(postToEdit, posts.get(0), newText) ||
                    !AssertPosts.postsEquals(post, posts.get(1))) {
                throw new AssertionFailedError();
            }
        } else if (posts.get(0).getId().equals(post.getId())) {
            if (!AssertPosts.postsEquals(postToEdit, posts.get(1), newText) ||
                    !AssertPosts.postsEquals(post, posts.get(0))) {
                throw new AssertionFailedError();
            }
        } else {
            throw new AssertionFailedError();
        }
    }
}