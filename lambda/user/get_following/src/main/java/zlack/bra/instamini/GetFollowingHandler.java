package zlack.bra.instamini;


import java.util.*;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.document.BatchGetItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.TableKeysAndAttributes;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;

import static java.util.Map.of;


public class GetFollowingHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;

    private final DynamoDB dynamoDB = InstanceModule.dynamoDB;

    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    /**
     * Gets all users, who user with userId follows
     * @param event all information about request
     * @param context request context
     * @return 200, if it was successful or 400, if query param userId is missing
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            Map<String, String> queryParameters = event.getQueryStringParameters();
            if (queryParameters.isEmpty() || !queryParameters.containsKey("userId")) {
                return response.withStatusCode(400).withBody("{\"errorMessage\":\"Missing query parameter userId\"}");
            }

            ScanRequest scanRequest = new ScanRequest()
                    .withTableName(TableName.followTable)
                    .withFilterExpression("#uWho = :userId")
                    .withExpressionAttributeValues(of(
                            ":userId", new AttributeValue(queryParameters.get("userId"))
                    ))
                    .withExpressionAttributeNames(of("#uWho", "userIdWho"));

            ScanResult scanResult = client.scan(scanRequest);
            List<String> resultUserIds = new ArrayList<>();
            for (Map<String, AttributeValue> item : scanResult.getItems()) {
                resultUserIds.add(item.get("userIdWhom").getS());
            }
            resultUserIds = new ArrayList<>(new HashSet<>(resultUserIds)); // delete duplicates

            if (resultUserIds.isEmpty()) {
                return response.withStatusCode(200).withBody("{}");
            }

            TableKeysAndAttributes keysAndAttributes =
                    new TableKeysAndAttributes(TableName.userTable);

            keysAndAttributes.addHashOnlyPrimaryKeys("id", resultUserIds.toArray());

            BatchGetItemOutcome outcome = dynamoDB.batchGetItem(keysAndAttributes);

            List<User> usersResult = new ArrayList<>();

            Map<String, KeysAndAttributes> unprocessed;

            do {
                for (String tableName : outcome.getTableItems().keySet()) {
                    List<Item> items = outcome.getTableItems().get(tableName);
                    for (Item item : items) {
                        usersResult.add(new User(item.getString("id"), item.getString("username"), item.getString("email")));
                    }
                }
                unprocessed = outcome.getUnprocessedKeys();

                if (!unprocessed.isEmpty()) {
                    outcome = dynamoDB.batchGetItemUnprocessed(unprocessed);
                }
            } while (!unprocessed.isEmpty());

            final String result = objectMapper.writeValueAsString(usersResult);
            return response.withStatusCode(200).withBody(result);
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
