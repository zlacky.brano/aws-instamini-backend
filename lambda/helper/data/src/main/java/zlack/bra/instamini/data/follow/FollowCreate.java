package zlack.bra.instamini.data.follow;

public final class FollowCreate {

    private String userIdWho;

    private String userIdWhom;

    public FollowCreate() {
    }

    public FollowCreate(String userIdWho, String userIdWhom) {
        this.userIdWho = userIdWho;
        this.userIdWhom = userIdWhom;
    }

    public String getUserIdWho() {
        return userIdWho;
    }

    public void setUserIdWho(String userIdWho) {
        this.userIdWho = userIdWho;
    }

    public String getUserIdWhom() {
        return userIdWhom;
    }

    public void setUserIdWhom(String userIdWhom) {
        this.userIdWhom = userIdWhom;
    }

    public String toJson() {
        return "{\"userIdWho\":\"" + userIdWho + "\", \"userIdWhom\":\"" + userIdWhom + "\"}";
    }
}
