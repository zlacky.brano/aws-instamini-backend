package zlack.bra.instamini.delete.exception;

public class DeleteChildrenException extends Exception {

    public DeleteChildrenException(String message) {
        super(message);
    }
}
