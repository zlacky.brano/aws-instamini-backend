package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.user.UserCreate;
import zlack.bra.instamini.instance.InstanceModule;

import java.util.*;


public class SaveUserHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;
    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    /**
     * Saves user
     * @param event all information about request
     * @param context request context
     * @return 201, if it was successful
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            UserCreate user = objectMapper.readValue(event.getBody(), UserCreate.class);

            Map<String, AttributeValue> userMap = new HashMap<>();
            userMap.put("id", new AttributeValue(UUID.randomUUID().toString()));
            userMap.put("username", new AttributeValue(user.getUsername()));
            userMap.put("email", new AttributeValue(user.getEmail()));

            PutItemRequest putItemRequest = new PutItemRequest()
                    .withTableName(TableName.userTable).withItem(userMap);

            client.putItem(putItemRequest);
            final String result = "{\"message\": \"User created with id " + userMap.get("id").getS() + "\"}";
            return response.withStatusCode(201).withBody(result);
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
