package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.*;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream;
import org.apache.commons.fileupload.MultipartStream;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.data.post.PostCreate;
import zlack.bra.instamini.exception.MissingBodyKeyException;
import zlack.bra.instamini.image.upload.CloudinaryService;
import zlack.bra.instamini.image.upload.ImageUploadService;
import zlack.bra.instamini.image.upload.exception.BodyDecodeException;
import zlack.bra.instamini.image.upload.exception.UploadException;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.CheckIfDocumentIdExists;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.*;


public class SavePostHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;
    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    private final ImageUploadService imageUploadService = new CloudinaryService();

    /**
     * Saves post
     * @param event all information about request
     * @param context request context
     * @return 201, if it was successful, 400, if content type is not correct or multipart body is not in right form, 404 if user was nout found
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);

        if (!event.getHeaders().containsKey("Content-Type") || !event.getHeaders().get("Content-Type").startsWith("multipart/form-data")) {
            return response.withStatusCode(400).withBody("{\"errorMessage\":\"Resource is accepting only multipart/form-data\"}");
        }

        try {
            PostCreate post = getPostCreateOrThrow(event);

            InvokeResult userGetByIdResult = CheckIfDocumentIdExists.checkIfUserExists(post.getUserId());
            if (userGetByIdResult.getFunctionError() != null) {
                return response.withStatusCode(500).withBody("{}");
            }

            int statusCodeUser = objectMapper.readTree(new ByteBufferBackedInputStream(userGetByIdResult.getPayload())).get("statusCode").asInt();
            if (statusCodeUser != 200) {
                return response.withStatusCode(statusCodeUser).withBody("{\"errorMessage\":\"User with userId does not exist or there was a server error.\"}");

            }
            String photoUrl = imageUploadService.upload(post.getImage());

            Map<String, AttributeValue> postMap = new HashMap<>();
            postMap.put("id", new AttributeValue(UUID.randomUUID().toString()));
            postMap.put("photoUrl", new AttributeValue(photoUrl));
            postMap.put("description", new AttributeValue(post.getDescription()));
            postMap.put("userId", new AttributeValue(post.getUserId()));
            postMap.put("time", new AttributeValue(LocalDateTime.now().toString()));

            PutItemRequest putItemRequest = new PutItemRequest()
                    .withTableName(TableName.postTable).withItem(postMap);

            client.putItem(putItemRequest);
            final String result = "{\"message\": \"Post created with id " + postMap.get("id").getS() + "\"}";
            return response.withStatusCode(201).withBody(result);
        } catch (final MissingBodyKeyException | UploadException | BodyDecodeException e) {
            return response.withStatusCode(400).withBody("{\"errorMessage\":\"" + e.getMessage() + "\"}");
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }

    private PostCreate getPostCreateOrThrow(APIGatewayProxyRequestEvent event) throws IOException, MissingBodyKeyException, BodyDecodeException {
        String description = null;
        String userId = null;
        byte[] image = null;

        byte[] decodedBytes;
        try {
            decodedBytes = Base64.getDecoder().decode(event.getBody().getBytes());
        } catch (IllegalArgumentException e) {
            throw new BodyDecodeException("Make sure that your multipart body has image, description and userId and it is in right form.");
        }
        Map<String, String> headersRequest = event.getHeaders();
        String contentType = headersRequest.get("Content-Type");

        String[] boundaryArray = contentType.split("=");
        byte[] boundary = boundaryArray[1].getBytes();

        ByteArrayInputStream content = new ByteArrayInputStream(decodedBytes);
        MultipartStream multipartStream = new MultipartStream(content, boundary, decodedBytes.length, null);
        boolean nextPart = multipartStream.skipPreamble();
        while (nextPart) {
            String header = multipartStream.readHeaders();

            ByteArrayOutputStream out = new ByteArrayOutputStream();
            multipartStream.readBodyData(out);
            nextPart = multipartStream.readBoundary();

            if (header.startsWith("Content-Disposition: form-data; name=\"userId\"")) {
                userId = out.toString();
            } else if (header.startsWith("Content-Disposition: form-data; name=\"description\"")) {
                description = out.toString();
            } else if (header.startsWith("Content-Disposition: form-data; name=\"image\";")) {
                image = out.toByteArray();
            }
        }

        if (description == null || image == null || userId == null) {
            throw new MissingBodyKeyException("Body must contain description, userId and image!");
        } else {
            return new PostCreate(description, userId, image);
        }
    }
}
