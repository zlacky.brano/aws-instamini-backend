package zlack.bra.instamini.test.item;

import com.amazonaws.services.dynamodbv2.document.Item;
import zlack.bra.instamini.data.comment.Comment;
import zlack.bra.instamini.data.follow.Follow;
import zlack.bra.instamini.data.like.Like;
import zlack.bra.instamini.data.post.Post;
import zlack.bra.instamini.data.user.User;

public class CreateItem {

    public static Item createCommentItem(Comment comment) {
        return new Item()
                .withPrimaryKey("id", comment.getId())
                .withString("userId", comment.getUserId())
                .withString("postId", comment.getPostId())
                .withString("time", comment.getTime())
                .withString("text", comment.getText());
    }

    public static Item createPostItem(Post post) {
        return new Item()
                .withPrimaryKey("id", post.getId())
                .withString("userId", post.getUserId())
                .withString("description", post.getDescription())
                .withString("time", post.getTime())
                .withString("photoUrl", post.getPhotoUrl());
    }

    public static Item createUserItem(User user) {
        return new Item()
                .withPrimaryKey("id", user.getId())
                .withString("username", user.getUsername())
                .withString("email", user.getEmail());
    }

    public static Item createFollowItem(Follow follow) {
        return new Item()
                .withPrimaryKey("id", follow.getId())
                .withString("userIdWho", follow.getUserIdWho())
                .withString("userIdWhom", follow.getUserIdWhom());
    }

    public static Item createLikeItem(Like like) {
        return new Item()
                .withPrimaryKey("id", like.getId())
                .withString("userId", like.getUserId())
                .withString("postId", like.getPostId());
    }
}
