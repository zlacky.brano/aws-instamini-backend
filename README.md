# Instamini backend
Inspired by Instagram

![Diagram](/diagram/AWS-backend.jpeg)

# Run

You need to have installed jq for processing JSON and Docker.

## Prerequisites

- [Docker](https://docs.docker.com/get-docker)
- [Docker Compose](https://docs.docker.com/compose/install/)

```sh
$ git clone https://gitlab.com/zlacky.brano/aws-instamini-backend.git
$ sudo apt-get install jq (if you do not have installed jq)
$ cd aws-instamini-backend
```

## Test lambdas (Optional)
```sh
$ ./gradlew test (requires Java 11)
```

## Build lambdas
```sh
$ bash ./scripts/code/lambdas_build_zip.sh (requires Java 11)
```

## Run localstack
```sh
$ docker-compose up localstack
```

## Create Lambda functions, Rest Api and DynamoDB tables with help of aws-cli
```sh
$ bash ./scripts/aws/setup_aws.sh
```

# Example of use with curl

In printed information from command above, you have 5 base URLs for User, Post, Comment, Like and Follow.

Each curl POST and PUT command has to have same form as stated below.

## User

##### Get users
```sh
$ curl <Base URL of User>
```

##### Get user by id
```sh
$ curl <Base URL of User>/{id}
```

##### Get user by email
```sh
$ curl <Base URL of User>/byEmail?email=<email>
```

##### Delete user by id (and all his posts, comments, follows and likes)
```sh
$ curl -X DELETE <Base URL of User>/{id}
```

##### Save user
```sh
$ curl -X POST -H "Content-Type: application/json" -d '{"username":"<username>", "email":"<email>"}' <Base URL of User>
```

##### Edit username of user by id
```sh
$ curl -X PUT -H "Content-Type: application/json" -d '{"username":"<username>"}' <Base URL of User>/{id}/editUsername
```

##### Get followers of user by id
```sh
$ curl <Base URL of User>/followers?userId=<userId>
```

##### Get users, who user with id follows
```sh
$ curl <Base URL of User>/following?userId=<userId>
```

##### Get count of followers of user by id
```sh
$ curl <Base URL of User>/followers/count?userId=<userId>
```

##### Get count of users, who user with id follows
```sh
$ curl <Base URL of User>/following/count?userId=<userId>
```

##### Get users, who liked post by id
```sh
$ curl <Base URL of User>/likedPost?postId=<postId>
```

##### Search users by username's prefix
```sh
$ curl <Base URL of User>/search?prefix=<prefix>
```

## Follow

##### Get follows
```sh
$ curl <Base URL of Follow>
```

##### Get follow by id
```sh
$ curl <Base URL of Follow>/{id}
```

##### Delete follow by id
```sh
$ curl -X DELETE <Base URL of Follow>/{id}
```

##### Save follow (users have to exist in database)
```sh
$ curl -X POST -H "Content-Type: application/json" -d '{"userIdWho":"<userIdWho>", "userIdWhom":"<userIdWhom>"}' <Base URL of Follow>
```

## Like

##### Get likes
```sh
$ curl <Base URL of Like>
```

##### Get like by id
```sh
$ curl <Base URL of Like>/{id}
```

##### Delete like by id
```sh
$ curl -X DELETE <Base URL of Like>/{id}
```

##### Save like (user and post have to exist in database)
```sh
$ curl -X POST -H "Content-Type: application/json" -d '{"userId":"<userId>", "postId":"<postId>"}' <Base URL of Like>
```

##### Get likes on post
```sh
$ curl <Base URL of Like>/onPost?postId=<postId>
```

##### Get number of likes on post
```sh
$ curl <Base URL of Like>/count?postId=<postId>
```

## Comment

##### Get comments
```sh
$ curl <Base URL of Comment>
```

##### Get comment by id
```sh
$ curl <Base URL of Comment>/{id}
```

##### Delete comment by id
```sh
$ curl -X DELETE <Base URL of Comment>/{id}
```

##### Save comment (user and post have to exist in database)
```sh
$ curl -X POST -H "Content-Type: application/json" -d '{"userId":"<userId>", "postId":"<postId>"}' <Base URL of Comment>
```

##### Get comments on post
```sh
$ curl <Base URL of Comment>/onPost?postId=<postId>
```

##### Edit text of comment by id
```sh
$ curl -X PUT -H "Content-Type: application/json" -d '{"text":"<text>"}' <Base URL of Comment>/{id}/editText
```

## Post

##### Get posts
```sh
$ curl <Base URL of Post>
```

##### Get post by id
```sh
$ curl <Base URL of Post>/{id}
```

##### Delete post by id (and all its comments and likes)
```sh
$ curl -X DELETE <Base URL of Post>/{id}
```

##### Save post (user has to exist in database)
```sh
$ curl -F 'userId=<userId>' -F 'description=<description>' -F 'image=@<path>' <Base URL of Post>
```

##### Edit description of comment by id
```sh
$ curl -X PUT -H "Content-Type: application/json" -d '{"description":"<description>"}' <Base URL of Post>/{id}/editDescription
```

##### Get posts of user
```sh
$ curl <Base URL of Post>/ofUser?userId=<userId>
```

##### Get number of posts of user
```sh
$ curl <Base URL of Post>/count?userId=<userId>
```
