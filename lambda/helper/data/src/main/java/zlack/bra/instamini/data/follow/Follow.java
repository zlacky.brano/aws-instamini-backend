package zlack.bra.instamini.data.follow;

public final class Follow {

    private String id;
    private String userIdWho;
    private String userIdWhom;

    public Follow() {
    }

    public Follow(String id, String userIdWho, String userIdWhom) {
        this.id = id;
        this.userIdWho = userIdWho;
        this.userIdWhom = userIdWhom;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUserIdWho() {
        return userIdWho;
    }

    public void setUserIdWho(String userIdWho) {
        this.userIdWho = userIdWho;
    }

    public String getUserIdWhom() {
        return userIdWhom;
    }

    public void setUserIdWhom(String userIdWhom) {
        this.userIdWhom = userIdWhom;
    }
}
