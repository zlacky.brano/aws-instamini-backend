package zlack.bra.instamini;

import java.util.*;

import com.amazonaws.services.dynamodbv2.document.*;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.user.User;
import zlack.bra.instamini.instance.InstanceModule;


public class SearchUsersHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final DynamoDB dynamoDB = InstanceModule.dynamoDB;

    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    private final Integer maxResults = 10;

    /**
     * Gets all users by given prefix of username
     * @param event all information about request
     * @param context request context
     * @return 200, if it was successful or 400, if query param prefix is missing
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            Table table = dynamoDB.getTable(TableName.userTable);

            Map<String, String> queryParameters = event.getQueryStringParameters();
            if (queryParameters.isEmpty() || !queryParameters.containsKey("prefix")) {
                return response.withStatusCode(400).withBody("{\"errorMessage\":\"Missing query parameter prefix\"}");
            }

            ItemCollection<ScanOutcome> scanResult = table.scan(new ScanFilter("username").beginsWith(queryParameters.get("prefix")));

            Iterator<Item> it = scanResult.firstPage().iterator();
            List<User> userResult = new ArrayList<>();
            for(int i = 0; i < scanResult.firstPage().size() && i < maxResults; i++) {
                Item item = it.next();
                User user = new User((String) item.get("id"), (String) item.get("username"), (String) item.get("email"));
                userResult.add(user);
            }
            final String resultString = objectMapper.writeValueAsString(userResult);
            return response.withStatusCode(200).withBody(resultString);
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
