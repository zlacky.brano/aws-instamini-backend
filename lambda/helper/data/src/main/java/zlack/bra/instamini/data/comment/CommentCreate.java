package zlack.bra.instamini.data.comment;

public final class CommentCreate {

    private String text;

    private String userId;

    private String postId;

    public CommentCreate() {
    }

    public CommentCreate(String text, String userId, String postId) {
        this.text = text;
        this.userId = userId;
        this.postId = postId;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String toJson() {
        return "{\"text\":\"" + text + "\", " +
                "\"userId\":\"" + userId + "\", " +
                "\"postId\":\"" + postId + "\"}";
    }
}
