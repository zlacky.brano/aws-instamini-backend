package zlack.bra.instamini.invoke;

import com.amazonaws.services.lambda.AWSLambda;
import com.amazonaws.services.lambda.model.InvokeRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import zlack.bra.instamini.instance.InstanceModule;

public final class CheckIfDocumentIdExists {
    
    private final static AWSLambda client = InstanceModule.awsLambda;

    /**
     * Invokes lambda to check, if user with given id exists
     * @param userId of user
     * @return result of invocation
     */
    public static InvokeResult checkIfUserExists(String userId) {
        String functionName = "GetUserByIdFunction";

        InvokeRequest request = new InvokeRequest();
        request.withFunctionName(functionName)
                .withPayload("{\"pathParameters\":{\"id\":\"" + userId + "\"}}");
        return client.invoke(request);
    }

    /**
     * Invokes lambda to check, if post with given id exists
     * @param postId of post
     * @return result of invocation
     */
    public static InvokeResult checkIfPostExists(String postId) {
        String functionName = "GetPostByIdFunction";

        InvokeRequest request = new InvokeRequest();
        request.withFunctionName(functionName)
                .withPayload("{\"pathParameters\":{\"id\":\"" + postId + "\"}}");
        return client.invoke(request);
    }
}
