package zlack.bra.instamini;

import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.model.AttributeValue;
import com.amazonaws.services.dynamodbv2.model.PutItemRequest;
import com.amazonaws.services.lambda.model.InvokeResult;
import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyRequestEvent;
import com.amazonaws.services.lambda.runtime.events.APIGatewayProxyResponseEvent;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.util.ByteBufferBackedInputStream;
import zlack.bra.instamini.config.TableName;
import zlack.bra.instamini.data.comment.CommentCreate;
import zlack.bra.instamini.instance.InstanceModule;
import zlack.bra.instamini.invoke.CheckIfDocumentIdExists;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;


public class SaveCommentHandler implements RequestHandler<APIGatewayProxyRequestEvent, APIGatewayProxyResponseEvent> {

    private final AmazonDynamoDB client = InstanceModule.amazonDynamoDB;
    private final ObjectMapper objectMapper = InstanceModule.objectMapper;

    /**
     * Saves comment
     * @param event all information about request
     * @param context request context
     * @return return 201, if it was successful or 404, if user or post was not found
     */
    public APIGatewayProxyResponseEvent handleRequest(final APIGatewayProxyRequestEvent event, final Context context) {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/json");

        APIGatewayProxyResponseEvent response = new APIGatewayProxyResponseEvent()
                .withHeaders(headers);
        try {
            CommentCreate comment = objectMapper.readValue(event.getBody(), CommentCreate.class);

            InvokeResult userGetByIdResult = CheckIfDocumentIdExists.checkIfUserExists(comment.getUserId());
            InvokeResult postGetByIdResult = CheckIfDocumentIdExists.checkIfPostExists(comment.getPostId());
            if (userGetByIdResult.getFunctionError() != null || postGetByIdResult.getFunctionError() != null) {
                return response.withStatusCode(500).withBody("{}");
            }

            int statusCodeUser = objectMapper.readTree(new ByteBufferBackedInputStream(userGetByIdResult.getPayload())).get("statusCode").asInt();
            int statusCodePost = objectMapper.readTree(new ByteBufferBackedInputStream(postGetByIdResult.getPayload())).get("statusCode").asInt();
            if (statusCodeUser != 200) {
                return response.withStatusCode(statusCodeUser).withBody("{\"errorMessage\":\"User with id userId does not exist or there was a server error.\"}");
            } else if (statusCodePost != 200) {
                return response.withStatusCode(statusCodePost).withBody("{\"errorMessage\":\"Post with id postId does not exist or there was a server error.\"}");
            }

            Map<String, AttributeValue> commentMap = new HashMap<>();
            commentMap.put("id", new AttributeValue(UUID.randomUUID().toString()));
            commentMap.put("text", new AttributeValue(comment.getText()));
            commentMap.put("userId", new AttributeValue(comment.getUserId()));
            commentMap.put("postId", new AttributeValue(comment.getPostId()));
            commentMap.put("time", new AttributeValue(LocalDateTime.now().toString()));

            PutItemRequest putItemRequest = new PutItemRequest()
                    .withTableName(TableName.commentTable).withItem(commentMap);

            client.putItem(putItemRequest);
            final String result = "{\"message\": \"Comment created with id " + commentMap.get("id").getS() + "\"}";
            return response.withStatusCode(201).withBody(result);
        } catch (final Exception e) {
            return response.withStatusCode(500).withBody("{}");
        }
    }
}
